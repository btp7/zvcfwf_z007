sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/model/Filter",
    "sap/ui/model/FilterOperator",
    "sap/ui/core/Fragment",
    "sap/m/Token",
    "sap/m/MessageToast",
    "sap/m/MessageBox"

],
    /**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    function (Controller, Filter, FilterOperator, Fragment, Token, MessageToast, MessageBox) {
        "use strict";

        return Controller.extend("zvcfwfz007workflowuimodule.controller.MyTaskUI", {
            onInit: function () {
                this.getView().setModel(
                    new sap.ui.model.json.JSONModel({
                        vaildinput: null,
                        visible: {
                            paymentmethod: false,
                            reconaccount: false
                        },
                        editable: {
                            paymentmethod: false,
                            reconaccount: false
                        }

                    })
                );

            },
            onAfterRendering: function () {
                this.getView().byId("idPage").scrollTo(0);
                var sFin_flag = this.getView().getModel("context").getProperty("/fin_flag");
                if (sFin_flag === true) {
                    this.getView().getModel().setProperty("/visible/paymentmethod", true);
                    this.getView().getModel().setProperty("/visible/reconaccount", true);
                    this.getView().getModel().setProperty("/editable/paymentmethod", true);
                    this.getView().getModel().setProperty("/editable/reconaccount", true);
                } else {
                    this.getView().getModel().setProperty("/visible/paymentmethod", false);
                    this.getView().getModel().setProperty("/visible/reconaccount", false);
                    this.getView().getModel().setProperty("/editable/paymentmethod", false);
                    this.getView().getModel().setProperty("/editable/reconaccount", false);
                }
            },
            onChange: function (oEvent) {
                var oInput = oEvent.getSource();
                this._validateInput(oInput);

                if (oEvent.getSource().getId().includes("idComboBoxAccountGroup") === true) {
                    if (oEvent.getSource().getValue() === "Z007") {
                        this.getView().byId("idInputGeneralAssignedVendorCode").setVisible(false);
                        this.getView().byId("idInputGeneralAssignedVendorCode").setEditable(false);
                    } else {
                        this.getView().byId("idInputGeneralAssignedVendorCode").setVisible(true);
                        this.getView().byId("idInputGeneralAssignedVendorCode").setEditable(true);
                    }
                }
            },

            _onvhRegion: function (oEvent) {
                if (!this._vhRegionDialog) {
                    this._vhRegionDialog = sap.ui.xmlfragment("zvcfwfz007workflowuimodule.fragment.vhRegion", this);
                    this.getView().addDependent(this._vhRegionDialog);
                }

                var aFilters = [];
                var afilter = new Filter("country", FilterOperator.EQ, this.getView().getModel().getProperty("/general/country"));
                aFilters.push(afilter);
                this._vhRegionDialog.getBinding("items").filter(aFilters);

                this._vhRegionDialog.open();
                this._vhID = oEvent.getSource().getId();
                var shId = this._vhRegionDialog._searchField.getId();
                $("#" + shId).hide();
            },
            _validateInput: function (oInput) {
                var sValueState = "None";
                var bValidationError = false;
                var oBinding = oInput.getBinding("value");

                if (oBinding.oValue == '' || oBinding.oValue == null) {
                    sValueState = "Error";
                    bValidationError = true;
                    this._onSetVaildInput(false);
                } else {
                    this._onSetVaildInput(true);
                }

                oInput.setValueState(sValueState);

                return bValidationError;
            },
            _onSetVaildInput: function (sInput) {
                this.getView().getModel().setProperty("/vaildinput", sInput);
            },
            onvhSupplyType: function () {
                this._oMultiInput = this.getView().byId("idMultiInputCompanySupplyType");

                this.oColModel = new sap.ui.model.json.JSONModel({
                    "cols": [
                        {
                            "label": "Supply Type",
                            "template": "supply_type"
                        },
                        {
                            "label": "Category",
                            "template": "category"
                        },
                        {
                            "label": "Description",
                            "template": "description"
                        }
                    ]
                });

                var aData = {};
                $.ajax({
                    type: "get",
                    async: false,
                    contentType: "application/json",
                    url: this.getOwnerComponent().getManifestObject().resolveUri("odata/v4/TableService/SuuplyTypes"),
                    dataType: "json",
                    cache: false,
                    success: function (data, textStatus, jqXHR) { aData = data.value; }
                });

                this.oItemModel = new sap.ui.model.json.JSONModel({
                    "data": aData
                });

                var aCols = this.oColModel.getData().cols;

                Fragment.load({
                    name: "zvcfwfz007workflowuimodule.fragment.vhSupplyType",
                    controller: this
                }).then(function name(oFragment) {
                    this._oValueHelpDialog = oFragment;
                    this.getView().addDependent(this._oValueHelpDialog);

                    this._oValueHelpDialog.getTableAsync().then(function (oTable) {
                        oTable.setModel(this.oItemModel);
                        oTable.setModel(this.oColModel, "columns");

                        if (oTable.bindRows) {
                            oTable.bindAggregation("rows", "/data");
                        }

                        if (oTable.bindItems) {
                            oTable.bindAggregation("items", "/data", function () {
                                return new ColumnListItem({
                                    cells: aCols.map(function (column) {
                                        return new Label({ text: "{" + column.template + "}" });
                                    })
                                });
                            });
                        }

                        this._oValueHelpDialog.update();
                    }.bind(this));

                    this._oValueHelpDialog.setTokens(this._oMultiInput.getTokens());
                    this._oValueHelpDialog.open();

                }.bind(this));

            },
            onValueHelpOkPress: function (oEvent) {
                var aTokens = oEvent.getParameter("tokens");
                var sKey = null,
                    sText = null,
                    sPath = null,
                    sPath_ = null;
                for (var i = 0; i < aTokens.length; i++) {
                    if (sKey) {
                        sKey = sKey + aTokens[i].getProperty("text").substr(0, 1);
                    } else {
                        sKey = aTokens[i].getProperty("text").substr(0, 1);
                    }

                    if (sText) {
                        sText = sText + "," + aTokens[i].getProperty("key");
                    } else {
                        sText = aTokens[i].getProperty("key");
                    }
                }

                var aCompany = this.getView().getModel("context").getProperty("/company");
                for (var i = 0; i < aCompany.length; i++) {
                    if (aCompany[i].company === this._sCompany) {
                        sPath = "/company/" + i + "/supplytype";
                        sPath_ = "/company/" + i + "/supplytypename";
                        this.getView().getModel("context").setProperty(sPath, sKey);
                        this.getView().getModel("context").setProperty(sPath_, sText);
                        break;
                    }
                }

                if (sKey) {
                    this.onCheckMultiInput();
                }

                this._oValueHelpDialog.close();
            },
            onValueHelpCancelPress: function () {
                this._oValueHelpDialog.close();
            },

            onValueHelpAfterClose: function () {
                this._oValueHelpDialog.destroy();
            },
            onCheckMultiInput: function () {
                var aInputs = [],
                    bValidationError = false;
                var ck = this.getView().getControlsByFieldGroupId("checkMultiInputs");
                for (var i = 0; i < ck.length; i++) {
                    if (ck[i].getMetadata().getElementName() !== "sap.m.MultiInput") {
                        continue;
                    }
                    else {
                        if (ck[i]._lastValue) {
                            ck[i].setValueState(sap.ui.core.ValueState.None);
                        }
                    }
                }
            },
            onTabIconTabBar: function (oEvent) {
                var sText = oEvent.getParameters().item.getProperty("text");
                if (sText === "Purchase View") {
                    this._sPurchaseOrg = this.getView().getModel("context").getProperty("/purchase")[0].purchaseorg;
                    this._sCountry = this.getView().getModel("context").getProperty("/general/country");
                    this.onSetLocalData();
                }
                if (sText === "Company View") {
                    this._sCompany = this.getView().getModel("context").getProperty("/company")[0].company;
                }
            },
            onSelectTabBarCompany: function (oEvent) {
                var oParmeters = oEvent.getParameters();
                this._sCompany = oParmeters.selectedItem.getProperty("text");
            },

            onSetLocalData: function () {
                var sfilter = "?$filter=(purchase_org eq '" + this._sPurchaseOrg + "') and (country_key eq '" + this._sCountry + "')";
                var aData = {};
                $.ajax({
                    type: "get",
                    async: false,
                    contentType: "application/json",
                    url: this.getOwnerComponent().getManifestObject().resolveUri("odata/v4/TableService/PurchaseOrgs") + sfilter,
                    dataType: "json",
                    cache: false,
                    success: function (data, textStatus, jqXHR) { aData = data.value; }
                });

                var ck = this.getView().getControlsByFieldGroupId("checkPur");
                if (aData.length === 0) {
                    for (var i = 0; i < ck.length; i++) {
                        if (ck[i].getMetadata().getElementName() !== "sap.m.Input") {
                            continue;
                        } else {
                            ck[i].setValueState(sap.ui.core.ValueState.None);
                        }

                        ck[i].setVisible(false);
                        ck[i].setEditable(false);
                    }

                } else {
                    for (var i = 0; i < ck.length; i++) {
                        if (ck[i].getMetadata().getElementName() === "sap.m.Input") {
                            ck[i].setVisible(true);
                            ck[i].setEditable(true);
                        }
                    }
                }
            },
            onSelectTabBarPurchase: function (oEvent) {
                var oParmeters = oEvent.getParameters();
                this._sPurchaseOrg = oParmeters.selectedItem.getProperty("text");
                this._sCountry = this.getView().getModel("context").getProperty("/general/country");
                this.onSetLocalData();
            },
            _onvhPaymentTerm: function () {
                this._oInput = this.getView().byId("idInputCompanyPaymentTerm");

                this.oColModel = new sap.ui.model.json.JSONModel({
                    "cols": [
                        {
                            "label": "Payment Term Key",
                            "template": "payment_term_key"
                        },
                        {
                            "label": "Payment Period",
                            "template": "payment_period"
                        },
                        {
                            "label": "Remark1",
                            "template": "remark1"
                        }
                    ]
                });

                var aData = {};
                $.ajax({
                    type: "get",
                    async: false,
                    contentType: "application/json",
                    url: this.getOwnerComponent().getManifestObject().resolveUri("odata/v4/TableService/PaymentTerms"),
                    dataType: "json",
                    cache: false,
                    success: function (data, textStatus, jqXHR) { aData = data.value; }
                });

                this.oItemModel = new sap.ui.model.json.JSONModel({
                    "data": aData
                });

                var aCols = this.oColModel.getData().cols;

                Fragment.load({
                    name: "zvcfwfz007workflowuimodule.fragment.vhPaymentTerm",
                    controller: this
                }).then(function name(oFragment) {
                    this._oValueHelpDialog = oFragment;
                    this.getView().addDependent(this._oValueHelpDialog);

                    this._oValueHelpDialog.getTableAsync().then(function (oTable) {
                        oTable.setModel(this.oItemModel);
                        oTable.setModel(this.oColModel, "columns");

                        if (oTable.bindRows) {
                            oTable.bindAggregation("rows", "/data");
                        }

                        if (oTable.bindItems) {
                            oTable.bindAggregation("items", "/data", function () {
                                return new ColumnListItem({
                                    cells: aCols.map(function (column) {
                                        return new Label({ text: "{" + column.template + "}" });
                                    })
                                });
                            });
                        }

                        this._oValueHelpDialog.update();
                    }.bind(this));

                    var oToken = new Token();
                    oToken.setKey(this._oInput.getSelectedKey());
                    oToken.setText(this._oInput.getValue());
                    this._oValueHelpDialog.setTokens([oToken]);
                    this._oValueHelpDialog.open();
                }.bind(this));
            },

            onValueHelpOkPressPT: function (oEvent) {
                var aTokens = oEvent.getParameter("tokens");
                var sKey = aTokens[0].getProperty("key"),
                    sPath = null;
                var aCompany = this.getView().getModel("context").getProperty("/company");
                for (var i = 0; i < aCompany.length; i++) {
                    if (aCompany[i].company === this._sCompany) {
                        sPath = "/company/" + i + "/paymentterm";
                        this.getView().getModel("context").setProperty(sPath, sKey);
                        break;
                    }
                }
                this._oValueHelpDialog.close();
            },
            onValueHelpCancelPressPT: function () {
                this._oValueHelpDialog.close();
            },
            onValueHelpAfterClosePT: function () {
                this._oValueHelpDialog.destroy();
            },
            onFilterBarSearchPT: function (oEvent) {
                var aSelectionSet = oEvent.getParameter("selectionSet");
                var aFilters = aSelectionSet.reduce(function (aResult, oControl) {
                    if (oControl.getSelectedItem().getKey()) {
                        aResult.push(new Filter({
                            path: oControl.sId,
                            operator: FilterOperator.EQ,
                            value1: oControl.getSelectedItem().getKey()
                        }));
                    }

                    return aResult;
                }, []);

                this._filterTable(new Filter({
                    filters: aFilters,
                    and: true
                }));
            },
            _filterTable: function (oFilter) {
                var oValueHelpDialog = this._oValueHelpDialog;

                oValueHelpDialog.getTableAsync().then(function (oTable) {
                    if (oTable.bindRows) {
                        oTable.getBinding("rows").filter(oFilter);
                    }

                    if (oTable.bindItems) {
                        oTable.getBinding("items").filter(oFilter);
                    }

                    oValueHelpDialog.update();
                });
            },

            onvhSupplyType1: function () {
                this._oMultiInput = this.getView().byId("idMultiInputPurchaseSupplyType");

                this.oColModel = new sap.ui.model.json.JSONModel({
                    "cols": [
                        {
                            "label": "Supply Type",
                            "template": "supply_type"
                        },
                        {
                            "label": "Category",
                            "template": "category"
                        },
                        {
                            "label": "Description",
                            "template": "description"
                        }
                    ]
                });

                var aData = {};
                $.ajax({
                    type: "get",
                    async: false,
                    contentType: "application/json",
                    url: this.getOwnerComponent().getManifestObject().resolveUri("odata/v4/TableService/SuuplyTypes"),
                    dataType: "json",
                    cache: false,
                    success: function (data, textStatus, jqXHR) { aData = data.value; }
                });

                this.oItemModel = new sap.ui.model.json.JSONModel({
                    "data": aData
                });

                var aCols = this.oColModel.getData().cols;

                Fragment.load({
                    name: "zvcfwfz007workflowuimodule.fragment.vhSupplyType1",
                    controller: this
                }).then(function name(oFragment) {
                    this._oValueHelpDialog = oFragment;
                    this.getView().addDependent(this._oValueHelpDialog);

                    this._oValueHelpDialog.getTableAsync().then(function (oTable) {
                        oTable.setModel(this.oItemModel);
                        oTable.setModel(this.oColModel, "columns");

                        if (oTable.bindRows) {
                            oTable.bindAggregation("rows", "/data");
                        }

                        if (oTable.bindItems) {
                            oTable.bindAggregation("items", "/data", function () {
                                return new ColumnListItem({
                                    cells: aCols.map(function (column) {
                                        return new Label({ text: "{" + column.template + "}" });
                                    })
                                });
                            });
                        }

                        this._oValueHelpDialog.update();
                    }.bind(this));

                    this._oValueHelpDialog.setTokens(this._oMultiInput.getTokens());
                    this._oValueHelpDialog.open();

                }.bind(this));
            },
            onValueHelpOkPress1: function (oEvent) {
                var aTokens = oEvent.getParameter("tokens");
                var sKey = null,
                    sText = null,
                    sPath = null,
                    sPath_ = null;
                for (var i = 0; i < aTokens.length; i++) {
                    if (sKey) {
                        sKey = sKey + aTokens[i].getProperty("text").substr(0, 1);
                    } else {
                        sKey = aTokens[i].getProperty("text").substr(0, 1);
                    }

                    if (sText) {
                        sText = sText + "," + aTokens[i].getProperty("key");
                    } else {
                        sText = aTokens[i].getProperty("key");
                    }
                }

                var aPurchase = this.getView().getModel("context").getProperty("/purchase");
                for (var i = 0; i < aPurchase.length; i++) {
                    if (aPurchase[i].purchaseorg === this._sPurchaseOrg) {
                        sPath = "/purchase/" + i + "/supplytype";
                        sPath_ = "/purchase/" + i + "/supplytypename";
                        this.getView().getModel("context").setProperty(sPath, sKey);
                        this.getView().getModel("context").setProperty(sPath_, sText);
                        break;
                    }
                }
                if (sKey) {
                    this.onCheckMultiInput();
                }

                this._oValueHelpDialog.close();
            },
            onValueHelpCancelPress1: function () {
                this._oValueHelpDialog.close();
            },

            onValueHelpAfterClose1: function () {
                this._oValueHelpDialog.destroy();
            },

            onhandleLinkPress: function (oEvent) {
                var sFileName = oEvent.getSource().getText();
                var sUrl = this.getOwnerComponent().getManifestObject().resolveUri("elements/api-v2/files?path=%2FGeneral%2F") + this.getView().getModel("context").getProperty("/no") + sFileName;
                fetch(sUrl, {
                    method: 'get',
                    headers: new Headers({
                        Subsite: "/teams/FTPforBTP",
                        /*    Authorization: "User m8yOTE3QbD0SlgsEQbYzj3lfMDYCaJ1I8wejhQKQXSY=, Organization e127a73fd11950b4fb9056750c67171a, Element c2ZLd2zsG0240Qe033m5jdSkI+CvaRwQtbrYr9SCk1s="*/
                    })
                })
                    .then(response => response.blob())
                    .then(blob => {
                        var url = window.URL.createObjectURL(blob);
                        var a = document.createElement('a');
                        a.href = url;
                        a.download = sFileName;
                        document.body.appendChild(a); // we need to append the element to the dom -> otherwise it will not work in firefox
                        a.click();
                        a.remove();  //afterwards we remove the element again 
                    }).catch(function (err) {
                        MessageToast.show(err);
                    })

            },

            onvhPaymentMethod: function () {
                this._oMultiInput = this.getView().byId("idMultiInputPaymentMethod");

                this.oColModel = new sap.ui.model.json.JSONModel({
                    "cols": [
                        {
                            "label": "Payment Method",
                            "template": "payment_method"
                        },
                        {
                            "label": "Description",
                            "template": "description"
                        }
                    ]
                });

                var aData = {};
                $.ajax({
                    type: "get",
                    async: false,
                    contentType: "application/json",
                    url: this.getOwnerComponent().getManifestObject().resolveUri("odata/v4/TableService/PaymentMethods"),
                    dataType: "json",
                    cache: false,
                    success: function (data, textStatus, jqXHR) { aData = data.value; }
                });

                this.oItemModel = new sap.ui.model.json.JSONModel({
                    "data": aData
                });

                var aCols = this.oColModel.getData().cols;

                Fragment.load({
                    name: "zvcfwfz007workflowuimodule.fragment.vhPaymentMethod",
                    controller: this
                }).then(function name(oFragment) {
                    this._oValueHelpDialog = oFragment;
                    this.getView().addDependent(this._oValueHelpDialog);

                    this._oValueHelpDialog.getTableAsync().then(function (oTable) {
                        oTable.setModel(this.oItemModel);
                        oTable.setModel(this.oColModel, "columns");

                        if (oTable.bindRows) {
                            oTable.bindAggregation("rows", "/data");
                        }

                        if (oTable.bindItems) {
                            oTable.bindAggregation("items", "/data", function () {
                                return new ColumnListItem({
                                    cells: aCols.map(function (column) {
                                        return new Label({ text: "{" + column.template + "}" });
                                    })
                                });
                            });
                        }

                        this._oValueHelpDialog.update();
                    }.bind(this));

                    this._oValueHelpDialog.setTokens(this._oMultiInput.getTokens());
                    this._oValueHelpDialog.open();

                }.bind(this));

            },
            onValueHelpOkPressPM: function (oEvent) {
                var aTokens = oEvent.getParameter("tokens");
                var sKey = null,
                    sText = null,
                    sPath = null,
                    sPath_ = null;
                for (var i = 0; i < aTokens.length; i++) {
                    if (sKey) {
                        sKey = sKey + aTokens[i].getProperty("text").substr(0, 1);
                    } else {
                        sKey = aTokens[i].getProperty("text").substr(0, 1);
                    }

                    /* if (sText) {
                        sText = sText + "," + aTokens[i].getProperty("key");
                    } else {
                        sText = aTokens[i].getProperty("key");
                    } */
                }

                var aCompany = this.getView().getModel("context").getProperty("/company");
                for (var i = 0; i < aCompany.length; i++) {
                    if (aCompany[i].company === this._sCompany) {
                        sPath = "/company/" + i + "/paymentmethod";
                        this.getView().getModel("context").setProperty(sPath, sKey);
                        break;
                    }
                }

                if (sKey) {
                    this.onCheckMultiInput();
                }

                this._oValueHelpDialog.close();
            },
            onValueHelpCancelPressPM: function () {
                this._oValueHelpDialog.close();
            },

            onValueHelpAfterClosePM: function () {
                this._oValueHelpDialog.destroy();
            },
        });
    });
