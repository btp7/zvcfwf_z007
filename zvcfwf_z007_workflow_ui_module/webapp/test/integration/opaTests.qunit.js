/* global QUnit */

sap.ui.require(["zvcfwfz007workflowuimodule/test/integration/AllJourneys"
], function () {
	QUnit.config.autostart = false;
	QUnit.start();
});
