/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"zvcfwf_z007_workflow_ui_module/test/unit/AllTests"
	], function () {
		QUnit.start();
	});
});
