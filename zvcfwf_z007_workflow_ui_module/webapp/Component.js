sap.ui.define(
    [
        "sap/ui/core/UIComponent",
        "sap/ui/Device",
        "zvcfwfz007workflowuimodule/model/models",
        "sap/m/MessageToast",
        "sap/m/MessageBox"
    ],
    function (UIComponent, Device, models, MessageToast, MessageBox) {
        "use strict";
        var sVaildinput = null;
        return UIComponent.extend(
            "zvcfwfz007workflowuimodule.Component",
            {
                metadata: {
                    manifest: "json",
                },

                /**
                 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
                 * @public
                 * @override
                 */

                init: function () {
                    // call the base component's init function
                    UIComponent.prototype.init.apply(this, arguments);

                    // enable routing
                    this.getRouter().initialize();

                    // set the device model
                    this.setModel(models.createDeviceModel(), "device");

                    this.setTaskModels();
                    this.getInboxAPI().addAction(
                        {
                            action: "APPROVE",
                            label: "Approve",
                            type: "accept", // (Optional property) Define for positive appearance
                        },
                        function () {
                            /*      if (this.onCheckInput()) { */
                            this.completeTask(true);
                            /*        } */
                        },
                        this
                    );

                    this.getInboxAPI().addAction(
                        {
                            action: "REJECT",
                            label: "Reject",
                            type: "reject", // (Optional property) Define for negative appearance
                        },
                        function () {
                            if (this.onCheckReason()) {
                                /*          if (this.onCheckInput()) { */
                                this.completeTask(false);
                                /*  } */
                            }debugger
                        },
                        this
                    );
                    /* debugger;
                    this.getModel("context").setProperty("/url", window.location.href); */
                },

                setTaskModels: function () {
                    // set the task model
                    var startupParameters = this.getComponentData().startupParameters;
                    this.setModel(startupParameters.taskModel, "task");

                    // set the task context model
                    var taskContextModel = new sap.ui.model.json.JSONModel(
                        this._getTaskInstancesBaseURL() + "/context"
                    );
                    this.setModel(taskContextModel, "context");
                },

                _getTaskInstancesBaseURL: function () {
                    return (
                        this._getWorkflowRuntimeBaseURL() +
                        "/task-instances/" +
                        this.getTaskInstanceID()
                    );
                },

                _getWorkflowRuntimeBaseURL: function () {
                    var appId = this.getManifestEntry("/sap.app/id");
                    var appPath = appId.replaceAll(".", "/");
                    var appModulePath = jQuery.sap.getModulePath(appPath);

                    return appModulePath + "/bpmworkflowruntime/v1";
                },

                getTaskInstanceID: function () {
                    return this.getModel("task").getData().InstanceID;
                },

                getInboxAPI: function () {
                    var startupParameters = this.getComponentData().startupParameters;
                    return startupParameters.inboxAPI;
                },

                completeTask: function (approvalStatus) {
                    this.getModel("context").setProperty("/approved", approvalStatus);
                    this._patchTaskInstance();
                    this._refreshTaskList();
                },

                _patchTaskInstance: function () {
                    var data = {
                        status: "COMPLETED",
                        context: this.getModel("context").getData(),
                    };

                    jQuery.ajax({
                        url: this._getTaskInstancesBaseURL(),
                        method: "PATCH",
                        contentType: "application/json",
                        async: false,
                        data: JSON.stringify(data),
                        headers: {
                            "X-CSRF-Token": this._fetchToken(),
                        },
                    });
                },

                _fetchToken: function () {
                    var fetchedToken;

                    jQuery.ajax({
                        url: this._getWorkflowRuntimeBaseURL() + "/xsrf-token",
                        method: "GET",
                        async: false,
                        headers: {
                            "X-CSRF-Token": "Fetch",
                        },
                        success(result, xhr, data) {
                            fetchedToken = data.getResponseHeader("X-CSRF-Token");
                        },
                    });
                    return fetchedToken;
                },

                _refreshTaskList: function () {
                    this.getInboxAPI().updateTask("NA", this.getTaskInstanceID());
                },

                onCheckInput: function () {
                    /*   debugger;
                      var aInputs = [],
                          bValidationError = false;
  
                      var ck = sap.ui.getCore().byFieldGroupId("checkInputs");
                      for (var i = 0; i < ck.length; i++) {
                          if ((ck[i].getMetadata().getElementName() !== "sap.m.Input") &&
                              (ck[i].getMetadata().getElementName() !== "sap.m.ComboBox") &&
                              (ck[i].getMetadata().getElementName() !== "sap.m.MultiInput")) {
                              continue;
                          }
                          else {
                              ck[i].setValueState(sap.ui.core.ValueState.None);
                          }
                          if (ck[i].getVisible() !== true ||
                              ck[i].getEditable() !== true) {
                              continue;
                          }
  
                          if (ck[i].getRequired() && ck[i].getValue() === "") {
                              aInputs.push(ck[i]);
                          }
                      }
                      aInputs.forEach(function (oInput) {
                          bValidationError = this._validateInput(oInput) || bValidationError;
                          if (bValidationError == true) {
                              this._onSetVaildInput(false);
                          }
                      }, this);
                      if (!bValidationError) {
                          return true;
                      } else {
                          MessageBox.alert(this._getText("message002"));
                          return false;
                      }
   */
                },
                _getText: function (sTextId, aArgs) {
                    return this.getModel("i18n").getResourceBundle().getText(sTextId, aArgs);
                },
                _validateInput: function (oInput) {
                    var sValueState = "None";
                    var bValidationError = false;
                    var oBinding = oInput.getBinding("value");

                    if (oBinding.oValue == '' || oBinding.oValue == null) {
                        sValueState = "Error";
                        bValidationError = true;
                        this._onSetVaildInput(false);
                    } else {
                        this._onSetVaildInput(true);
                    }

                    oInput.setValueState(sValueState);

                    return bValidationError;
                },

                _onSetVaildInput: function (sInput) {
                    sVaildinput = sInput;
                },
                onCheckReason: function () {
                    if (this.getModel("context").getProperty("/reason")) {
                        return true;
                    } else {
                        MessageBox.alert(this._getText("message001"));
                        return false;
                    }
                }

            }
        );
    }
);
