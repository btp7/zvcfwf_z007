/*
// read from existing workflow context 
var productInfo = $.context.productInfo; 
var productName = productInfo.productName; 
var productDescription = productInfo.productDescription;

// read contextual information
var taskDefinitionId = $.info.taskDefinitionId;

// read user task information
var lastUserTask1 = $.usertasks.usertask1.last;
var userTaskSubject = lastUserTask1.subject;
var userTaskProcessor = lastUserTask1.processor;
var userTaskCompletedAt = lastUserTask1.completedAt;

var userTaskStatusMessage = " User task '" + userTaskSubject + "' has been completed by " + userTaskProcessor + " at " + userTaskCompletedAt;

// create new node 'product'
var product = {
		productDetails: productName  + " " + productDescription,
		workflowStep: taskDefinitionId
};

// write 'product' node to workflow context
$.context.product = product;
*/

/* $.context.log_request = $.context.subworkflow_response; */
var appldate = $.context.appldate;
appldate = appldate.substr(0, 10);
$.context.general_key = "(application_no='" + $.context.no + "',pic_id='Jerry',appliciant_id='testid')";
$.context.general_input = {
    "application_status": "status",
    "pic": $.context.pic,
    "appliciant": $.context.applicant,
    "application_date": appldate,
    "comment": $.context.reason,
    "reason": $.context.reason,
    "vendor_account_group": $.context.general.accountgroup,
    "vendor_code": $.context.vendorid,
    "vendor_name1": $.context.general.vendorname1,
    "vendor_name2": $.context.general.vendorname2,
    "vendor_address1": $.context.general.vendoraddress1,
    "vendor_address2": $.context.general.vendoraddress2,
    "vendor_address3": $.context.general.vendoraddress3,
    "vendor_address4": $.context.general.vendoraddress4,
    "vendor_address5": $.context.general.vendoraddress5,
    "search_term1": $.context.general.searchterm1,
    "search_term2": $.context.general.searchterm2,
    "post_code": $.context.general.postcode,
    "country_key": $.context.general.country,
    "language_key": $.context.general.language,
    "vat_no": $.context.general.vatno,
    "tax_no1": $.context.general.taxno,
    "city": $.context.general.city,
    "telephon": $.context.general.telephone,
    "fax": $.context.general.fax,
    "reigon": $.context.general.region

}

