/*
// read from existing workflow context 
var productInfo = $.context.productInfo; 
var productName = productInfo.productName; 
var productDescription = productInfo.productDescription;

// read contextual information
var taskDefinitionId = $.info.taskDefinitionId;

// read user task information
var lastUserTask1 = $.usertasks.usertask1.last;
var userTaskSubject = lastUserTask1.subject;
var userTaskProcessor = lastUserTask1.processor;
var userTaskCompletedAt = lastUserTask1.completedAt;

var userTaskStatusMessage = " User task '" + userTaskSubject + "' has been completed by " + userTaskProcessor + " at " + userTaskCompletedAt;

// create new node 'product'
var product = {
		productDetails: productName  + " " + productDescription,
		workflowStep: taskDefinitionId
};

// write 'product' node to workflow context
$.context.product = product;
*/

/* $.context.headquarter_input = {
    "input": {
        "application_id": "1",
        "vendor_account_group": "Z007",
        "applicant": "10910110",
        "company_purchase": [
            {
                "company": "L600",
                "purchase_org": "PWHQ",
                "supply_type": "N",
                "payment_term": "YTTP",
                "payment_period": 90
            },
            {
                "company": "L130",
                "purchase_org": "PWZS",
                "supply_type": "M",
                "payment_term": "Y130",
                "payment_period": 30
            }
        ]
    }
}; */


$.context.headquarter_input = {
    "input": {
        "application_id": $.context.no,
        "vendor_account_group": $.context.general.accountgroup,
        "applicant": $.context.applicant,
        "company_purchase": $.context.company_purchase
    }
};

