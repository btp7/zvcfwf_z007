/*
// read from existing workflow context 
var productInfo = $.context.productInfo; 
var productName = productInfo.productName; 
var productDescription = productInfo.productDescription;

// read contextual information
var taskDefinitionId = $.info.taskDefinitionId;

// read user task information
var lastUserTask1 = $.usertasks.usertask1.last;
var userTaskSubject = lastUserTask1.subject;
var userTaskProcessor = lastUserTask1.processor;
var userTaskCompletedAt = lastUserTask1.completedAt;

var userTaskStatusMessage = " User task '" + userTaskSubject + "' has been completed by " + userTaskProcessor + " at " + userTaskCompletedAt;

// create new node 'product'
var product = {
		productDetails: productName  + " " + productDescription,
		workflowStep: taskDefinitionId
};

// write 'product' node to workflow context
$.context.product = product;
*/
if ($.context.role == 0) {
    $.context.leader_reason = $.context.reason;
    $.context.reason = "";
    $.context.role = 1;

} else {
    if ($.context.mro == true) {
        if ($.context.role == 1) {
            $.context.mro_reason = $.context.reason;
            $.context.reason = "";
            $.context.role = 2;

        } else {
            if ($.context.role == 2) {
                $.context.fin_reason = $.context.reason;
                $.context.reason = "";
                $.context.role = 3;

            }
        }
    } else {
        if ($.context.role == 1) {
            $.context.fin_reason = $.context.reason;
            $.context.reason = "";
            $.context.role = 3;

        }
    }

}


