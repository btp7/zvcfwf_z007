/*
// read from existing workflow context 
var productInfo = $.context.productInfo; 
var productName = productInfo.productName; 
var productDescription = productInfo.productDescription;

// read contextual information
var taskDefinitionid = $.info.taskDefinitionid;

// read user task information
var lastUserTask1 = $.usertasks.usertask1.last;
var userTaskSubject = lastUserTask1.subject;
var userTaskProcessor = lastUserTask1.processor;
var userTaskCompletedAt = lastUserTask1.completedAt;

var userTaskStatusMessage = " User task '" + userTaskSubject + "' has been completed by " + userTaskProcessor + " at " + userTaskCompletedAt;

// create new node 'product'
var product = {
		productDetails: productName  + " " + productDescription,
		workflowStep: taskDefinitionid
};

// write 'product' node to workflow context
$.context.product = product;
*/

var aData = [];
var sPic = $.context.pic;
if (sPic == "Jerry") {
    aData = {
        workflow1: {
            applicant: {
                employee_id: "10910110",
                name: "Anderson Tsai",
                email: "Jerry.tc.hu@ibm.com"
            },
            supervisor: {
                employee_id: "8910110",
                name: "Anderson Tsai",
                email: "Jerry.tc.hu@ibm.com"
            },
            mro_managers: [
                {
                    employee_id: "95050110",
                    name: "Anderson Tsai",
                    email: "Jerry.tc.hu@ibm.com"
                },
                {
                    employee_id: "950501101",
                    name: "Anderson Tsai1",
                    email: "Jerry.tc.hu@ibm.com"
                }
            ],
            hq_fin: {
                employee_id: "8910110",
                name: "Anderson Tsai",
                email: "Jerry.tc.hu@ibm.com"
            }
        },
        workflow2: [
            {
                company_purchase: [
                    {
                        purchase_org: "PWHQ",
                        company: "L600"
                    },
                    {
                        purchase_org: "PWIH",
                        company: "L600"
                    }
                ],
                site_fin:
                {
                    employee_id: "9910110",
                    name: "Anderson_Tsai",
                    email: "Jerry.tc.hu@ibm.com"
                }
                ,
                managers: [
                    {
                        seq: "1",
                        employee_id: "9910111",
                        name: "Anderson_Tsai",
                        email: "Jerry.tc.hu@ibm.com"
                    },
                    {
                        seq: "2",
                        employee_id: "9910116",
                        name: "Anderson_Tsai",
                        email: "Jerry.tc.hu@ibm.com"
                    }
                ]
            },
            {
                company_purchase: [
                    {
                        purchase_org: "PWMX",
                        company: "L550"
                    },
                    {
                        purchase_org: "PWIH",
                        company: "L600"
                    }
                ],
                site_fin:
                {
                    employee_id: "991011F",
                    name: "ANDERSON TSAI",
                    email: "Jerry.tc.hu@ibm.com"
                }
                ,
                managers: [
                    {
                        seq: "1",
                        employee_id: "9910111",
                        name: "Anderson_Tsai",
                        email: "Jerry.tc.hu@ibm.com"
                    },
                    {
                        seq: "2",
                        employee_id: "9910112",
                        name: "Anderson_Tsai",
                        email: "Jerry.tc.hu@ibm.com"
                    },
                    {
                        seq: "3",
                        employee_id: "9910113",
                        name: "Anderson_Tsai",
                        email: "Jerry.tc.hu@ibm.com"
                    }
                ]
            }
        ]
    };
} else {
    aData = $.context.headquarter_output;
}

$.context.workflow = aData;
$.context.workflow1 = aData.workflow1;
$.context.workflow2 = aData.workflow2;
$.context.workflow1_leader = aData.workflow1.supervisor.email;
/* $.context.workflow1_mro = aData.workflow1.mro_managers.email; */
$.context.workflow1_mro = aData.workflow1.mro_managers;
$.context.workflow1_fin = aData.workflow1.hq_fin.email;
$.context.workflow2_length = aData.workflow2.length;
$.context.role = 0;
$.context.sub_workflow_i = 0;

if (aData.workflow1.mro_managers) {
    $.context.mro = true;
} else {
    $.context.mro = false;
}



/* $.context.workflowinfo = $.info.workflowInstanceId; */

