/*
// read from existing workflow context 
var productInfo = $.context.productInfo; 
var productName = productInfo.productName; 
var productDescription = productInfo.productDescription;

// read contextual information
var taskDefinitionId = $.info.taskDefinitionId;

// read user task information
var lastUserTask1 = $.usertasks.usertask1.last;
var userTaskSubject = lastUserTask1.subject;
var userTaskProcessor = lastUserTask1.processor;
var userTaskCompletedAt = lastUserTask1.completedAt;

var userTaskStatusMessage = " User task '" + userTaskSubject + "' has been completed by " + userTaskProcessor + " at " + userTaskCompletedAt;

// create new node 'product'
var product = {
		productDetails: productName  + " " + productDescription,
		workflowStep: taskDefinitionId
};

// write 'product' node to workflow context
$.context.product = product;
*/

/* $.context.general_input = {
    BatchID: "20220704143701",
    SystemName: "VCF",
    UserID: "VCF",
    ProcessType: "ZBADE008",
    Status: "",
    ErrorCode: "",
    ErrorMessage: "",
    JsonData: "{\"HEADER\":[{\"MANDT\":\"055\",\"ZCNUMBER\":\"CBG2016082900001\",\"ZGENDCNT\":00003,\"ZVATNCNT\":0,\"ZCOMPCNT\":0,\"ZBANKCNT\":0,\"ZWHLDCNT\":0,\"ZPURCCNT\":0,\"ZBLKCNT\":00001,\"ZSOURCE\":\"J\",\"ZVCFIMDAT\":20160829,\"ZVCFIMTIM\":113029,\"LIFNR\":\"\",\"ZSAPSTATS\":\"E\",\"ZSAPREFLG\":\"X\",\"ZSAPREDAT\":20160829,\"ZSAPRETIM\":113029}]}"
} */
var sBatchID = Math.random().toString().substr(2, 14);

var aWorkflow = [
    {
        "MANDT": "",
        "ZCNUMBER": sBatchID,
        "ZWFINSID": $.info.workflowInstanceId,
        "ZDEFID": "general"

    }
];

/* var aHeader = [
    {
        "ZCNUMBER": sBatchID,
        "ZCPUDT": new Date().toISOString().split('T')[0].replaceAll("-", ""),
        "ZGENDCNT": 1,
        "ZVATNCNT": 1,
        "ZCOMPCNT": 0,
        "ZBANKCNT": 0,
        "ZWHLDCNT": 0,
        "ZPURCCNT": 0,
        "ZBLKCNT": 0,
        "ZSOURCE": "J"
    }
]; */

var sLifnr = $.context.general.assignedvendorcode;
if (!sLifnr) {
    sLifnr = "DUMMY";
}

var aGeneral = [
    {
        "MANDT": "",
        "ZCNUMBER": sBatchID,
        "LIFNR": sLifnr,
        "ZACTION": "I",
        "KTOKK": $.context.general.accountgroup,
        "NAME1": $.context.general.vendorname1,
        "NAME2": $.context.general.vendorname2,
        "NAME3": $.context.general.vendoraddress1,
        "NAME4": $.context.general.vendoraddress2,
        "SORT1": $.context.general.searchterm1,
        "SORT2": $.context.general.searchterm2,
        "STREET": "",
        "STR_SUPPL1": $.context.general.vendoraddress3,
        "STR_SUPPL2": $.context.general.vendoraddress4,
        "STR_SUPPL3": $.context.general.vendoraddress5,
        "CITY1": $.context.general.city,
        "POST_CODE1": $.context.general.postcode,
        "COUNTRY": $.context.general.country,
        "TEL_NUMBER": $.context.general.telephone,
        "FAX_NUMBER": $.context.general.fax,
        "KUNNR": "",
        "VBUND": "",
        "KONZS": "",
        "STCEG": $.context.general.vatno,
        "DONT_USE_S": $.context.general.language,
        "STCD1": $.context.general.taxno,
        "REGIO": $.context.general.region,
        "SPERM": ""
    }
];

var aVAT = [];
if ($.context.general.vatno) {
    aVAT = [
        {
            "MANDT": "",
            "ZCNUMBER": sBatchID,
            "LIFNR": sLifnr,
            "LAND1": $.context.general.country,
            "ZACTION": "I",
            "STCEG": $.context.general.vatno
        }
    ]
};

var aHeader = [
    {
        "ZCNUMBER": sBatchID,
        "ZCPUDT": new Date().toISOString().split('T')[0].replaceAll("-", ""),
        "ZGENDCNT": aGeneral.length,
        "ZVATNCNT": aVAT.length,
        "ZCOMPCNT": 0,
        "ZBANKCNT": 0,
        "ZWHLDCNT": 0,
        "ZPURCCNT": 0,
        "ZBLKCNT": 0,
        "ZSOURCE": "J"
    }
];

var aJsonData = {
    "WORKFLOW": aWorkflow,
    "HEADER": aHeader,
    "GENERAL": aGeneral,
    "VAT": aVAT,
    "COMPANY": [],
    "TAX": [],
    "BANK": [],
    "PURCHASE": [],
    "BLOCK": []
};

aJsonData = JSON.stringify(aJsonData);
aJsonData = aJsonData.replace('"', '\"');

/* var stringified = {
    "BatchID": sBatchID,
    "SystemName": "VCF",
    "UserID": "VCF",
    "ProcessType": "ZBADE007",
    "Status": "",
    "ErrorCode": "",
    "ErrorMessage": "",
    "JsonData": aJsonData
};

$.context.general_test = {
    data: stringified
} */



$.context.erp_input = {
    "BatchID": sBatchID,
    "SystemName": "VCF",
    "UserID": "VCF",
    "ProcessType": "ZBADE007",
    "Status": "",
    "ErrorCode": "",
    "ErrorMessage": "",
    "JsonData": aJsonData
}

/* $.context.general_input1 = {
    BatchID: "20220704143701",
    SystemName: "VCF",
    UserID: "VCF",
    ProcessType: "ZBADE007",
    Status: "",
    ErrorCode: "",
    ErrorMessage: "",
    JsonData: "{\"WORKFLOW\":[{\"MANDT\":\"055\",\"ZCNUMBER\":\"20220627130001\",\"ZWFINSID\":\"202207211610\",\"ZDEFID\":\"202207211610\"}],\"HEADER\":[{\"ZCNUMBER\":\"CBG2016082900001\",\"ZCPUDT\":00000000,\"ZGENDCNT\":00003,\"ZVATNCNT\":0,\"ZCOMPCNT\":0,\"ZBANKCNT\":0,\"ZWHLDCNT\":0,\"ZPURCCNT\":0,\"ZBLKCNT\":00001,\"ZSOURCE\":\"J\"}],\"GENERAL\":[{\"MANDT\":\"055\",\"ZCNUMBER\":\"CBG2016082900001\",\"LIFNR\":\"AAA\",\"ZACTION\":\"C\",\"KTOKK\":\"Z003\",\"NAME1\":\"AAA\",\"NAME2\":\"AAA-2\",\"NAME3\":\"\",\"NAME4\":\"\",\"SORT1\":\"AAA\",\"SORT2\":\"\",\"STREET\":\"\",\"STR_SUPPL1\":\"\",\"STR_SUPPL2\":\"\",\"STR_SUPPL3\":\"\",\"CITY1\":\"\",\"POST_CODE1\":\"\",\"COUNTRY\":\"TW\",\"TEL_NUMBER\":\"\",\"FAX_NUMBER\":\"\",\"KUNNR\":\"\",\"VBUND\":\"\",\"KONZS\":\"\",\"STCEG\":\"\",\"DONT_USE_S\":\"\",\"STCD1\":\"\",\"REGIO\":\"\",\"SPERM\":\"\"},{\"MANDT\":\"055\",\"ZCNUMBER\":\"CBG2016082900001\",\"LIFNR\":\"AAB\",\"ZACTION\":\"C\",\"KTOKK\":\"Z003\",\"NAME1\":\"AAB\",\"NAME2\":\"AAB-1\",\"NAME3\":\"\",\"NAME4\":\"\",\"SORT1\":\"AAB\",\"SORT2\":\"AAB S2\",\"STREET\":\"STREET\",\"STR_SUPPL1\":\"STR_SUPPL1\",\"STR_SUPPL2\":\"STR_SUPPL2\",\"STR_SUPPL3\":\"STR_SUPPL3\",\"CITY1\":\"CITY1\",\"POST_CODE1\":\"1233\",\"COUNTRY\":\"ZZ\",\"TEL_NUMBER\":\"\",\"FAX_NUMBER\":\"\",\"KUNNR\":\"\",\"VBUND\":\"\",\"KONZS\":\"\",\"STCEG\":\"\",\"DONT_USE_S\":\"ZF\",\"STCD1\":\"\",\"REGIO\":\"\",\"SPERM\":\"\"}],\"VAT\":[],\"COMPANY\":[],\"TAX\":[],\"BANK\":[],\"PURCHASE\":[],\"BLOCK\":[{\"MANDT\":\"055\",\"ZCNUMBER\":\"CBG2016082900001\",\"LIFNR\":\"0000123133\",\"ZCATGRY\":\"C\",\"ZVALUE\":\"L330\",\"ZACTION\":\"U\"}]}"
}    */