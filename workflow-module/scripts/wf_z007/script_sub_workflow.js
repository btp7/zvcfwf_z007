/*
// read from existing workflow context 
var productInfo = $.context.productInfo; 
var productName = productInfo.productName; 
var productDescription = productInfo.productDescription;

// read contextual information
var taskDefinitionId = $.info.taskDefinitionId;

// read user task information
var lastUserTask1 = $.usertasks.usertask1.last;
var userTaskSubject = lastUserTask1.subject;
var userTaskprocessor = lastUserTask1.processor;
var userTaskCompletedAt = lastUserTask1.completedAt;

var userTaskStatusMessage = " User task '" + userTaskSubject + "' has been completed by " + userTaskprocessor + " at " + userTaskCompletedAt;

// create new node 'product'
var product = {
		productDetails: productName  + " " + productDescription,
		workflowStep: taskDefinitionId
};

// write 'product' node to workflow context
$.context.product = product;
*/
var i = $.context.sub_workflow_i;
var sub_workflow = $.context.workflow2;
var company_purchase = $.context.workflow2[i].company_purchase;
var company = [],
    purchase = [];
/* $.context.company_purchase = company_purchase; */
for (var a = 0; a < company_purchase.length; a++) {
    company.push(company_purchase[a].company);
    purchase.push(company_purchase[a].purchase_org);
}
/* $.context.company_purchase_company = company;
$.context.company_purchase_purchase = purchase; */

company = company.filter(function (item, pos) {
    return company.indexOf(item) == pos;
})
purchase = purchase.filter(function (item, pos) {
    return purchase.indexOf(item) == pos;
})
/* $.context.company_purchase_company1 = company;
$.context.company_purchase_purchase1 = purchase; */

var aCompany = $.context.company;
var aPurchase = $.context.purchase;
var aCompany_filter = [];
var aPurchase_filter = [];

for (var b = 0; b < company.length; b++) {
    for (var a = 0; a < aCompany.length; a++) {
        if (company[b] == aCompany[a].company) {
            aCompany_filter.push(aCompany[a]);
        }
    }
}

for (var c = 0; c < purchase.length; c++) {
    for (var a = 0; a < aPurchase.length; a++) {
        if (purchase[c] == aPurchase[a].purchaseorg) {
            aPurchase_filter.push(aPurchase[a]);
        }
    }
}
/* $.context.company_purchase_company2 = aCompany_filter;
$.context.company_purchase_purchase2 = aPurchase_filter; */
$.context.company_length = aCompany_filter.length;
$.context.purchase_length = aPurchase_filter.length;

var aContext = {
    vendorid: $.context.vendorid,
    companyid: $.context.companyid,
    purchaseid: $.context.purchaseid,
    processor: $.context.processor,
    pic: $.context.pic,
    appldate: $.context.appldate,
    applicant: $.context.applicant,
    no: $.context.no,
    preno: $.context.preno,
    reason: $.context.reason,
    general: $.context.general,
    company: aCompany_filter,
    purchase: aPurchase_filter,
    company_length: $.context.company_length,
    purchase_length: $.context.purchase_length,
    sub_workflow: sub_workflow[i],
    sub_workflow_managers_i: 0,
    attach: $.context.attach
    //testmail: "Jerry.tc.hu@ibm.com"
}

var request = {
    definitionId: "zvcf.wf_z007_sub",
    context: aContext
};

$.context.subworkflow_request = request; 