/*
// read from existing workflow context 
var productInfo = $.context.productInfo; 
var productName = productInfo.productName; 
var productDescription = productInfo.productDescription;

// read contextual information
var taskDefinitionId = $.info.taskDefinitionId;

// read user task information
var lastUserTask1 = $.usertasks.usertask1.last;
var userTaskSubject = lastUserTask1.subject;
var userTaskProcessor = lastUserTask1.processor;
var userTaskCompletedAt = lastUserTask1.completedAt;

var userTaskStatusMessage = " User task '" + userTaskSubject + "' has been completed by " + userTaskProcessor + " at " + userTaskCompletedAt;

// create new node 'product'
var product = {
		productDetails: productName  + " " + productDescription,
		workflowStep: taskDefinitionId
};

// write 'product' node to workflow context
$.context.product = product;
*/


var sBatchID = Math.random().toString().substr(2, 14);
var aWorkflow = [
    {
        "MANDT": "",
        "ZCNUMBER": sBatchID,
        "ZWFINSID": $.info.workflowInstanceId,
        "ZDEFID": "compur"

    }
];

var sLifnr = $.context.general.assignedvendorcode;
if (!sLifnr) {
    sLifnr = "DUMMY";
}
var aComapnyData = $.context.company,
    aComapny = [],
    sCompany = "",
    aBank = [],
    sBank = "",
    iBankCount = 0;

for (var a = 0; a < aComapnyData.length; a++) {
    sCompany = {
        "MANDT": "",
        "ZCNUMBER": sBatchID,
        "LIFNR": sLifnr,
        "BUKRS": aComapnyData[a].company,
        "ZACTION": "I",
        "AKONT": aComapnyData[a].reconaccount,
        "QLAND": "",
        "ZTERM": aComapnyData[a].paymentterm,
        "ZWELS": aComapnyData[a].paymentmethod,
        "BUSAB": "",
        "ZSABE": "",
        "TLFNS": "",
        "TLFXS": "",
        "INTAD": "",
        "KVERM": "",
        "J_1ICSTNO": "",
        "J_1ILSTNO": ""
    };
    aComapny.push(sCompany);

    if (aComapnyData[a].bankswiftcode) {
        iBankCount += 1;
        sBank = {
            "MANDT": "",
            "ZCNUMBER": sBatchID,
            "LIFNR": sLifnr,
            "BUKRS": aComapnyData[a].company,
            "ZZSEQ": iBankCount,
            "ZZBANKL": aComapnyData[a].bankswiftcode,
            "ZZBANKN": aComapnyData[a].bankaccount,
            "ZZWAERS": aComapnyData[a].bankcurrency,
            "ZACTION": "I",
            "ZZBENENAME": aComapnyData[a].beneificiaryname,
            "ZZRANK": "1",
            "ZZCPNAME": aComapnyData[a].contactperson,
            "ZZCPSMTP": aComapnyData[a].contactemail,
            "ZZFIELDA": aComapnyData[a].beneificiaryname12,
            "ZZFIELDB": aComapnyData[a].info
        };
        aBank.push(sBank);
    }

    if (aComapnyData[a].bankswiftcode2) {
        iBankCount += 1;
        sBank = {
            "MANDT": "",
            "ZCNUMBER": sBatchID,
            "LIFNR": sLifnr,
            "BUKRS": aComapnyData[a].company,
            "ZZSEQ": iBankCount,
            "ZZBANKL": aComapnyData[a].bankswiftcode2,
            "ZZBANKN": aComapnyData[a].bankaccount2,
            "ZZWAERS": aComapnyData[a].bankcurrency2,
            "ZACTION": "I",
            "ZZBENENAME": aComapnyData[a].beneficiaryname2,
            "ZZRANK": "1",
            "ZZCPNAME": aComapnyData[a].contactperson2,
            "ZZCPSMTP": aComapnyData[a].contactemail2,
            "ZZFIELDA": aComapnyData[a].beneificiaryname12,
            "ZZFIELDB": aComapnyData[a].info2
        };
        aBank.push(sBank);
    }

}

var aPurchaseData = $.context.purchase,
    aPurchase = [],
    sPurchase = "";

for (var b = 0; b < aPurchaseData.length; b++) {
    sPurchase = {
        "MANDT": "",
        "ZCNUMBER": sBatchID,
        "LIFNR": sLifnr,
        "EKORG": aPurchaseData[b].purchaseorg,
        "ZACTION": "I",
        "WAERS": aPurchaseData[b].pocurrency,
        "ZTERM": aPurchaseData[b].paymentterm,
        "INCO1": aPurchaseData[b].incoterm1,
        "INCO2": aPurchaseData[b].incoterm2,
        "KZRET": "",
        "VERKF": "",
        "TELF1": "",
        "ZLOCNAM1": aPurchaseData[b].localname1,
        "ZLOCNAM2": aPurchaseData[b].localname2,
        "ZLOCADDR1": aPurchaseData[b].localaddress1,
        "ZLOCADDR2": aPurchaseData[b].localaddress2,
        "ZLOCPOST": aPurchaseData[b].localpostcode,
        "ZLOCCITY": aPurchaseData[b].localcity,
        "ZLOCOUNTRY": aPurchaseData[b].localcountry,
        "ZPVLANKEY": aPurchaseData[b].language,
        "ZZECVR": "",
        "ZZVCFM": "",
        "ZZCSUM": "",
        "ZZBRND": "",
        "ZZDUNS": "",
        "ZZMWSKZ": aPurchaseData[b].taxcode,
        "LIFN2": "",
        "KALSK": ""
    };
    aPurchase.push(sPurchase);
}

var aHeader = [
    {
        "ZCNUMBER": sBatchID,
        "ZCPUDT": new Date().toISOString().split('T')[0].replaceAll("-", ""),
        "ZGENDCNT": 0,
        "ZVATNCNT": 0,
        "ZCOMPCNT": aComapnyData.length,
        "ZBANKCNT": aBank.length,
        "ZWHLDCNT": 0,
        "ZPURCCNT": aPurchaseData.length,
        "ZBLKCNT": 0,
        "ZSOURCE": "J"
    }
];

var aJsonData = {
    "WORKFLOW": aWorkflow,
    "HEADER": aHeader,
    "GENERAL": [],
    "VAT": [],
    "COMPANY": aComapny,
    "TAX": [],
    "BANK": aBank,
    "PURCHASE": aPurchase,
    "BLOCK": []
};

aJsonData = JSON.stringify(aJsonData);
aJsonData = aJsonData.replace('"', '\"');

$.context.erp_input = {
    "BatchID": sBatchID,
    "SystemName": "VCF",
    "UserID": "VCF",
    "ProcessType": "ZBADE007",
    "Status": "",
    "ErrorCode": "",
    "ErrorMessage": "",
    "JsonData": aJsonData
}