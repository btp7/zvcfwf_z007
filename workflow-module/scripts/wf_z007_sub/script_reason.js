/*
// read from existing workflow context 
var productInfo = $.context.productInfo; 
var productName = productInfo.productName; 
var productDescription = productInfo.productDescription;

// read contextual information
var taskDefinitionId = $.info.taskDefinitionId;

// read user task information
var lastUserTask1 = $.usertasks.usertask1.last;
var userTaskSubject = lastUserTask1.subject;
var userTaskProcessor = lastUserTask1.processor;
var userTaskCompletedAt = lastUserTask1.completedAt;

var userTaskStatusMessage = " User task '" + userTaskSubject + "' has been completed by " + userTaskProcessor + " at " + userTaskCompletedAt;

// create new node 'product'
var product = {
		productDetails: productName  + " " + productDescription,
		workflowStep: taskDefinitionId
};

// write 'product' node to workflow context
$.context.product = product;


*/
var i = $.context.sub_workflow_managers_i;
var sReason = $.context.reason;
$.context.reason = "";
var aManagers_reason = $.context.managers_reason;

if (aManagers_reason == null) {
    var aManagers_reason = [];
}

var sContent = {
    seq: $.context.sub_workflow.managers[i].seq,
    employee_id: $.context.sub_workflow.managers[i].employee_id,
    email: $.context.sub_workflow.managers[i].email,
    reason: sReason
};
aManagers_reason.push(sContent);
$.context.managers_reason = aManagers_reason;





