/*
// read from existing workflow context 
var productInfo = $.context.productInfo; 
var productName = productInfo.productName; 
var productDescription = productInfo.productDescription;

// read contextual information
var taskDefinitionId = $.info.taskDefinitionId;

// read user task information
var lastUserTask1 = $.usertasks.usertask1.last;
var userTaskSubject = lastUserTask1.subject;
var userTaskProcessor = lastUserTask1.processor;
var userTaskCompletedAt = lastUserTask1.completedAt;

var userTaskStatusMessage = " User task '" + userTaskSubject + "' has been completed by " + userTaskProcessor + " at " + userTaskCompletedAt;

// create new node 'product'
var product = {
		productDetails: productName  + " " + productDescription,
		workflowStep: taskDefinitionId
};

// write 'product' node to workflow context
$.context.product = product;
*/

var purchase_length = $.context.purchase_length;
var aPurchase = $.context.purchase;
var appldate = $.context.appldate;
appldate = appldate.substr(0, 10);
$.context.purchase_key = "(application_no='" + $.context.no + "',pic_id='Jerry',appliciant_id='testid',purchase_org='" + aPurchase[purchase_length].purchaseorg + "')";
$.context.purchase_input = {
    "application_status": "",
    "pic": $.context.pic,
    "appliciant": $.context.applicant,
    "application_date": appldate,
    "comment": $.context.reason,
    "reason": $.context.reason,
    "origin_application_no": null,
    "po_currency": aPurchase[purchase_length].pocurrency,
    "supply_type": aPurchase[purchase_length].supplytype,
    "payment_term": aPurchase[purchase_length].paymentterm,
    "pr_type": null,
    "inco_tem1":aPurchase[purchase_length].incoterm1,
    "inco_term2": aPurchase[purchase_length].incoterm2,
    "tax_code": aPurchase[purchase_length].taxcode,
    "language_key": aPurchase[purchase_length].language
};
