/*
// read from existing workflow context 
var productInfo = $.context.productInfo; 
var productName = productInfo.productName; 
var productDescription = productInfo.productDescription;

// read contextual information
var taskDefinitionId = $.info.taskDefinitionId;

// read user task information
var lastUserTask1 = $.usertasks.usertask1.last;
var userTaskSubject = lastUserTask1.subject;
var userTaskProcessor = lastUserTask1.processor;
var userTaskCompletedAt = lastUserTask1.completedAt;

var userTaskStatusMessage = " User task '" + userTaskSubject + "' has been completed by " + userTaskProcessor + " at " + userTaskCompletedAt;

// create new node 'product'
var product = {
		productDetails: productName  + " " + productDescription,
		workflowStep: taskDefinitionId
};

// write 'product' node to workflow context
$.context.product = product;
*/
var manager_flag = "";
var managers = $.context.sub_workflow.managers;
/* $.context.sub_workflow_managers = $.context.sub_workflow.managers;
$.context.sub_workflow_managers_length = managers.length; */
if (managers.length > 0) {
    manager_flag = true;
} else {
    manager_flag = false;
    $.context.site_fin_mail = $.context.sub_workflow.site_fin.email;
}
$.context.manager_flag = manager_flag;