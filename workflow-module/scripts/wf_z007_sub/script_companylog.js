/*
// read from existing workflow context 
var productInfo = $.context.productInfo; 
var productName = productInfo.productName; 
var productDescription = productInfo.productDescription;

// read contextual information
var taskDefinitionId = $.info.taskDefinitionId;

// read user task information
var lastUserTask1 = $.usertasks.usertask1.last;
var userTaskSubject = lastUserTask1.subject;
var userTaskProcessor = lastUserTask1.processor;
var userTaskCompletedAt = lastUserTask1.completedAt;

var userTaskStatusMessage = " User task '" + userTaskSubject + "' has been completed by " + userTaskProcessor + " at " + userTaskCompletedAt;

// create new node 'product'
var product = {
		productDetails: productName  + " " + productDescription,
		workflowStep: taskDefinitionId
};

// write 'product' node to workflow context
$.context.product = product;
*/
var company_length = $.context.company_length;
var aCompany = $.context.company;
var appldate = $.context.appldate;
appldate = appldate.substr(0, 10);
$.context.company_key = "(application_no='" + $.context.no + "',pic_id='Jerry',appliciant_id='testid',company='" + aCompany[company_length].company + "')";
$.context.company_input = {
    "application_status": "",
    "pic": $.context.pic,
    "appliciant": $.context.applicant,
    "application_date": appldate,
    "comment": $.context.reason,
    "reason": $.context.reason,
    "origin_application_no": null,
    "supply_type": aCompany[company_length].supplytype,
    "payment_term": aCompany[company_length].paymentterm,
    "payment_method": aCompany[company_length].paymentmethod,
    "recon_account": aCompany[company_length].reconaccount,
    "language_key": aCompany[company_length].languagekey,
    "bank_currency": aCompany[company_length].bankcurrency,
    "bank_wift_code": aCompany[company_length].bankswiftcode,
    "bank_account": aCompany[company_length].bankaccount,
    "beneficiary_name": aCompany[company_length].beneificiaryname,
    "beneficiary_name12": aCompany[company_length].beneificiaryname12,
    "info": aCompany[company_length].info,
    "contact_person": aCompany[company_length].contactperson,
    "contact_email": aCompany[company_length].contactemail,
    "bank_currency2": aCompany[company_length].bankcurrency2,
    "bank_wift_code2": aCompany[company_length].bankswiftcode2,
    "bank_account2": aCompany[company_length].bankaccount2,
    "beneficiary_name2": aCompany[company_length].beneficiaryname2,
    "beneficiary_name22": aCompany[company_length].beneficiaryname22,
    "info2": aCompany[company_length].info2,
    "contact_person2": aCompany[company_length].contactperson2,
    "contact_email2": aCompany[company_length].contactemail2

};
