{
	"contents": {
		"f34f4f8c-fca6-41d1-80a0-c174e9c4711c": {
			"classDefinition": "com.sap.bpm.wfs.Model",
			"id": "zvcf.wf_z007",
			"subject": "wf_z007",
			"name": "wf_z007",
			"documentation": "",
			"lastIds": "62d7f4ed-4063-4c44-af8b-39050bd44926",
			"events": {
				"11a9b5ee-17c0-4159-9bbf-454dcfdcd5c3": {
					"name": "StartEvent1"
				},
				"2798f4e7-bc42-4fad-a248-159095a2f40a": {
					"name": "EndEvent1"
				},
				"a751b391-32be-4389-ab13-2f353ea70664": {
					"name": "End"
				},
				"4ec004d0-7c1d-4d8e-8a2f-e90c6324d8ce": {
					"name": "End"
				},
				"b87ffcc4-8b99-4ab4-87ef-c289c3b980ab": {
					"name": "End"
				},
				"6bef400e-2b19-4d0d-8f45-a620ba9a4e17": {
					"name": "End"
				},
				"e91eaa37-85ec-4f93-aaed-7a7bb6d0194b": {
					"name": "wait message"
				}
			},
			"activities": {
				"e57b0ff1-0a95-4fe2-aaee-8e7e8318a74f": {
					"name": "headquarter output data"
				},
				"02e5eb98-e5e2-4cd9-88f6-b8fc98cef8e2": {
					"name": "Application for Vendor Code Form(Leader)"
				},
				"4edcface-205d-4caa-beb3-81af7a23066d": {
					"name": "leader gateway"
				},
				"c1e12728-d6f8-415a-83ee-81c9a2429c51": {
					"name": "Leader Reason"
				},
				"b5add7d0-fe41-4f7a-8efa-ecf8efc41340": {
					"name": "MRO Gateway"
				},
				"ee4537c0-4cdb-49a6-aac1-723697faf785": {
					"name": "Application for Vendor Code Form(HQ FIN)"
				},
				"6db96a69-3a63-43f7-89e4-f3915c3a4b4d": {
					"name": "FIN Reason"
				},
				"bd671f55-7d6a-4b99-bc40-7a311fea31c5": {
					"name": "FIN Gateway"
				},
				"1647a344-4f9d-41bf-a0ca-273199e56001": {
					"name": "MRO flag"
				},
				"40a6913c-e571-457a-b014-a16373cc5d02": {
					"name": "general flag"
				},
				"39ff9ee6-f3b9-4d4b-a030-54cf9cd5e9b5": {
					"name": "general gateway"
				},
				"68941912-6b70-4117-a1c2-99247d119bc7": {
					"name": "workflow count"
				},
				"39eb65af-b2ee-4fc1-93bc-371e295a9cda": {
					"name": "sub workflow"
				},
				"37e55584-faed-48ac-a490-5b6df225cf7a": {
					"name": "sub workflow data"
				},
				"5b3bed6c-5129-47ea-a26f-30a817187831": {
					"name": "sub workflow data process"
				},
				"1bab0a79-dcb1-4a0d-9455-82f39d1740f7": {
					"name": "check count"
				},
				"b70dac8d-4a75-4b35-975f-b82c457a5a6e": {
					"name": "General log"
				},
				"dbb3ff87-4ec2-410a-9c1c-f588e0cda1f3": {
					"name": "ERP"
				},
				"3c648857-5ceb-4d60-a5c9-e4818b90dc31": {
					"name": "ERP data"
				},
				"ce471956-fc40-4c60-8050-9146f93df90b": {
					"name": "Mail"
				},
				"3876a36e-1803-4ec6-8b6f-3642194dc590": {
					"name": "Mail"
				},
				"a26cb84d-d2d1-4f6e-9f50-26cc707cd8df": {
					"name": "headquarter input data"
				},
				"7eb37dee-e558-4fd6-8ab2-0e07115865a0": {
					"name": "headquarter service"
				},
				"dcbd8a8f-eee7-4df5-9c54-f98911b5bb70": {
					"name": "General service"
				},
				"416d148a-5fb4-4788-95a2-60387070c39a": {
					"name": "Application for Vendor Code Form(MRO)"
				}
			},
			"sequenceFlows": {
				"59e9cbd1-baa4-4b13-aaec-e62241c009dd": {
					"name": "SequenceFlow3"
				},
				"c6efcbdb-74c3-4be7-b907-5ff83a4bff77": {
					"name": "SequenceFlow5"
				},
				"487ce51c-b9bb-43a9-a116-51fd856c6220": {
					"name": "approve"
				},
				"5174a1f9-e38a-449b-b4ab-c32b61e97ff9": {
					"name": "reject"
				},
				"28f51e6f-1c67-4505-b170-9db53a7accd0": {
					"name": "SequenceFlow9"
				},
				"fc3c6038-3f1c-4f93-a451-84d8033615fc": {
					"name": "approve"
				},
				"41f45834-fe4b-4763-afcf-7f44281315fa": {
					"name": "reject"
				},
				"a5549d68-5e9c-4189-9393-a691a04f4995": {
					"name": "SequenceFlow13"
				},
				"95a8228d-d0c0-4685-b002-a0e1add3b034": {
					"name": "SequenceFlow14"
				},
				"5cab1523-07e5-4695-801c-6dcbdbf0025d": {
					"name": "reject"
				},
				"4be2af38-9460-4029-814a-8607edf651c9": {
					"name": "MRO"
				},
				"316a1ad3-e80d-4870-b4c1-d5dfe07b2556": {
					"name": "None MRO"
				},
				"2f899641-ab75-4df4-b3cb-3c536efb079d": {
					"name": "created"
				},
				"4ce0c818-962f-4438-8e9b-afe15770c2be": {
					"name": "created failed"
				},
				"f6af5035-046d-4ddd-8d4d-c8c4d832c73f": {
					"name": ">0"
				},
				"8c837f09-e938-4544-ace4-3f6cc8441b8d": {
					"name": "=0"
				},
				"c7c38c0c-a8af-4ddd-8f48-8190362e387f": {
					"name": "SequenceFlow24"
				},
				"d27a48b3-a8cd-4116-b949-c3cfa67dff40": {
					"name": "SequenceFlow25"
				},
				"3c0d1c11-dc5e-41bd-bcaa-b3cb606739a4": {
					"name": "SequenceFlow26"
				},
				"d9c43d98-1678-491d-9ea1-e0bbf1fe49b0": {
					"name": "end"
				},
				"56d58e6e-4776-4075-80de-e1b610e56f29": {
					"name": "call sub workflow"
				},
				"6ab79f0c-6f91-4005-b6ec-a69ae0f83f2d": {
					"name": "SequenceFlow32"
				},
				"983b2103-7f26-4810-90c2-5358809316de": {
					"name": "SequenceFlow35"
				},
				"050bb5a2-825e-4d19-851b-be9735863da1": {
					"name": "SequenceFlow36"
				},
				"3755b090-3267-48b9-b6aa-55fef70b312a": {
					"name": "SequenceFlow37"
				},
				"dd23ed62-4671-4e2e-8484-e2a5778ebb87": {
					"name": "SequenceFlow38"
				},
				"2dee1e62-bf0e-4a3c-b128-fa0daea4347f": {
					"name": "SequenceFlow39"
				},
				"401c19ed-24d5-42ab-8cbd-008af9c7f346": {
					"name": "SequenceFlow43"
				},
				"7277a0e0-d563-40ab-a052-3af19e9ed5e8": {
					"name": "SequenceFlow45"
				},
				"4d568e9c-87e6-400a-a79f-5e34adab0c87": {
					"name": "SequenceFlow50"
				},
				"62653910-bd79-4d50-9383-088db4501b83": {
					"name": "SequenceFlow56"
				},
				"9a29462a-43ce-41f8-aa4f-747d8fb0ba8d": {
					"name": "SequenceFlow57"
				},
				"8ea517c9-a129-481b-bd9a-f9c836921f3c": {
					"name": "SequenceFlow60"
				},
				"779df3cb-4d98-4204-9e76-f9a86a54ce08": {
					"name": "SequenceFlow61"
				}
			},
			"diagrams": {
				"42fa7a2d-c526-4a02-b3ba-49b5168ba644": {}
			}
		},
		"11a9b5ee-17c0-4159-9bbf-454dcfdcd5c3": {
			"classDefinition": "com.sap.bpm.wfs.StartEvent",
			"id": "startevent1",
			"name": "StartEvent1"
		},
		"2798f4e7-bc42-4fad-a248-159095a2f40a": {
			"classDefinition": "com.sap.bpm.wfs.EndEvent",
			"id": "endevent1",
			"name": "EndEvent1"
		},
		"a751b391-32be-4389-ab13-2f353ea70664": {
			"classDefinition": "com.sap.bpm.wfs.EndEvent",
			"id": "endevent2",
			"name": "End",
			"eventDefinitions": {
				"cc88bc85-6ff4-4ea1-a229-572ea27be3c6": {}
			}
		},
		"4ec004d0-7c1d-4d8e-8a2f-e90c6324d8ce": {
			"classDefinition": "com.sap.bpm.wfs.EndEvent",
			"id": "endevent3",
			"name": "End",
			"eventDefinitions": {
				"66d8f718-151e-4944-8732-e2fd6c046c89": {}
			}
		},
		"b87ffcc4-8b99-4ab4-87ef-c289c3b980ab": {
			"classDefinition": "com.sap.bpm.wfs.EndEvent",
			"id": "endevent4",
			"name": "End",
			"eventDefinitions": {
				"fb30b137-aaf4-4520-9749-ae90d4094480": {}
			}
		},
		"6bef400e-2b19-4d0d-8f45-a620ba9a4e17": {
			"classDefinition": "com.sap.bpm.wfs.EndEvent",
			"id": "endevent5",
			"name": "End",
			"eventDefinitions": {
				"e505fe4b-cc38-4b8c-8951-6f29bdb28df3": {}
			}
		},
		"e91eaa37-85ec-4f93-aaed-7a7bb6d0194b": {
			"classDefinition": "com.sap.bpm.wfs.IntermediateCatchEvent",
			"id": "intermediatemessageevent4",
			"name": "wait message",
			"eventDefinitions": {
				"2c1763cd-e863-46e2-84b2-d5ea1ec463de": {}
			}
		},
		"e57b0ff1-0a95-4fe2-aaee-8e7e8318a74f": {
			"classDefinition": "com.sap.bpm.wfs.ScriptTask",
			"reference": "/scripts/wf_z007/script_service.js",
			"id": "scripttask1",
			"name": "headquarter output data"
		},
		"02e5eb98-e5e2-4cd9-88f6-b8fc98cef8e2": {
			"classDefinition": "com.sap.bpm.wfs.UserTask",
			"subject": "Leader",
			"priority": "MEDIUM",
			"isHiddenInLogForParticipant": false,
			"supportsForward": true,
			"userInterface": "sapui5://zvcfwf_z007_approuter.zvcfwfz007workflowuimodule/zvcfwfz007workflowuimodule",
			"recipientUsers": "${context.workflow1_leader}",
			"id": "usertask2",
			"name": "Application for Vendor Code Form(Leader)"
		},
		"4edcface-205d-4caa-beb3-81af7a23066d": {
			"classDefinition": "com.sap.bpm.wfs.ExclusiveGateway",
			"id": "exclusivegateway1",
			"name": "leader gateway",
			"default": "5174a1f9-e38a-449b-b4ab-c32b61e97ff9"
		},
		"c1e12728-d6f8-415a-83ee-81c9a2429c51": {
			"classDefinition": "com.sap.bpm.wfs.ScriptTask",
			"reference": "/scripts/wf_z007/script_reason.js",
			"id": "scripttask2",
			"name": "Leader Reason"
		},
		"b5add7d0-fe41-4f7a-8efa-ecf8efc41340": {
			"classDefinition": "com.sap.bpm.wfs.ExclusiveGateway",
			"id": "exclusivegateway2",
			"name": "MRO Gateway",
			"default": "fc3c6038-3f1c-4f93-a451-84d8033615fc"
		},
		"ee4537c0-4cdb-49a6-aac1-723697faf785": {
			"classDefinition": "com.sap.bpm.wfs.UserTask",
			"subject": "HQ FIN",
			"priority": "MEDIUM",
			"isHiddenInLogForParticipant": false,
			"supportsForward": true,
			"userInterface": "sapui5://zvcfwf_z007_approuter.zvcfwfz007workflowuimodule/zvcfwfz007workflowuimodule",
			"recipientUsers": "${context.workflow1_fin}",
			"id": "usertask4",
			"name": "Application for Vendor Code Form(HQ FIN)"
		},
		"6db96a69-3a63-43f7-89e4-f3915c3a4b4d": {
			"classDefinition": "com.sap.bpm.wfs.ScriptTask",
			"reference": "/scripts/wf_z007/script_reason.js",
			"id": "scripttask4",
			"name": "FIN Reason"
		},
		"bd671f55-7d6a-4b99-bc40-7a311fea31c5": {
			"classDefinition": "com.sap.bpm.wfs.ExclusiveGateway",
			"id": "exclusivegateway3",
			"name": "FIN Gateway",
			"default": "5cab1523-07e5-4695-801c-6dcbdbf0025d"
		},
		"1647a344-4f9d-41bf-a0ca-273199e56001": {
			"classDefinition": "com.sap.bpm.wfs.ExclusiveGateway",
			"id": "exclusivegateway4",
			"name": "MRO flag",
			"default": "316a1ad3-e80d-4870-b4c1-d5dfe07b2556"
		},
		"40a6913c-e571-457a-b014-a16373cc5d02": {
			"classDefinition": "com.sap.bpm.wfs.ScriptTask",
			"reference": "/scripts/wf_z007/script_general.js",
			"id": "scripttask6",
			"name": "general flag"
		},
		"39ff9ee6-f3b9-4d4b-a030-54cf9cd5e9b5": {
			"classDefinition": "com.sap.bpm.wfs.ExclusiveGateway",
			"id": "exclusivegateway5",
			"name": "general gateway",
			"default": "4ce0c818-962f-4438-8e9b-afe15770c2be"
		},
		"68941912-6b70-4117-a1c2-99247d119bc7": {
			"classDefinition": "com.sap.bpm.wfs.ExclusiveGateway",
			"id": "exclusivegateway6",
			"name": "workflow count",
			"default": "8c837f09-e938-4544-ace4-3f6cc8441b8d"
		},
		"39eb65af-b2ee-4fc1-93bc-371e295a9cda": {
			"classDefinition": "com.sap.bpm.wfs.ServiceTask",
			"destination": "zvcfwf_z007_sub",
			"destinationSource": "consumer",
			"path": "/v1/workflow-instances",
			"httpMethod": "POST",
			"requestVariable": "${context.subworkflow_request}",
			"responseVariable": "${context.subworkflow_response}",
			"id": "servicetask2",
			"name": "sub workflow"
		},
		"37e55584-faed-48ac-a490-5b6df225cf7a": {
			"classDefinition": "com.sap.bpm.wfs.ScriptTask",
			"reference": "/scripts/wf_z007/script_sub_workflow.js",
			"id": "scripttask7",
			"name": "sub workflow data"
		},
		"5b3bed6c-5129-47ea-a26f-30a817187831": {
			"classDefinition": "com.sap.bpm.wfs.ScriptTask",
			"reference": "/scripts/wf_z007/script_sub_workflow_data.js",
			"id": "scripttask8",
			"name": "sub workflow data process"
		},
		"1bab0a79-dcb1-4a0d-9455-82f39d1740f7": {
			"classDefinition": "com.sap.bpm.wfs.ExclusiveGateway",
			"id": "exclusivegateway7",
			"name": "check count",
			"default": "56d58e6e-4776-4075-80de-e1b610e56f29"
		},
		"b70dac8d-4a75-4b35-975f-b82c457a5a6e": {
			"classDefinition": "com.sap.bpm.wfs.ScriptTask",
			"reference": "/scripts/wf_z007/script_log.js",
			"id": "scripttask10",
			"name": "General log"
		},
		"dbb3ff87-4ec2-410a-9c1c-f588e0cda1f3": {
			"classDefinition": "com.sap.bpm.wfs.ServiceTask",
			"destination": "SAP_dev",
			"destinationSource": "consumer",
			"path": "/sap/opu/odata/sap/zcomservice/ZCOMCollection?sap-client=055",
			"httpMethod": "POST",
			"xsrfPath": "/sap/opu/odata/sap/zcomservice/ZCOMCollection?sap-client=055",
			"requestVariable": "${context.erp_input}",
			"responseVariable": "${context.erp_output}",
			"headers": [],
			"id": "servicetask4",
			"name": "ERP"
		},
		"3c648857-5ceb-4d60-a5c9-e4818b90dc31": {
			"classDefinition": "com.sap.bpm.wfs.ScriptTask",
			"reference": "/scripts/wf_z007/script_general_data.js",
			"id": "scripttask11",
			"name": "ERP data"
		},
		"ce471956-fc40-4c60-8050-9146f93df90b": {
			"classDefinition": "com.sap.bpm.wfs.MailTask",
			"destinationSource": "consumer",
			"id": "mailtask2",
			"name": "Mail",
			"mailDefinitionRef": "b845ca33-1b7a-409f-bfea-610d0354707e"
		},
		"3876a36e-1803-4ec6-8b6f-3642194dc590": {
			"classDefinition": "com.sap.bpm.wfs.MailTask",
			"destinationSource": "consumer",
			"id": "mailtask5",
			"name": "Mail",
			"mailDefinitionRef": "bcc3ebb6-9119-4f93-9063-e99a3726f82e"
		},
		"a26cb84d-d2d1-4f6e-9f50-26cc707cd8df": {
			"classDefinition": "com.sap.bpm.wfs.ScriptTask",
			"reference": "/scripts/wf_z007/script_headquater.js",
			"id": "scripttask14",
			"name": "headquarter input data"
		},
		"7eb37dee-e558-4fd6-8ab2-0e07115865a0": {
			"classDefinition": "com.sap.bpm.wfs.ServiceTask",
			"destination": "VCF_Vendor",
			"destinationSource": "consumer",
			"path": "/odata/v4/GetApprovalListService/getApprovalList",
			"httpMethod": "POST",
			"xsrfPath": "",
			"requestVariable": "${context.headquarter_input}",
			"responseVariable": "${context.headquarter_output}",
			"id": "servicetask8",
			"name": "headquarter service"
		},
		"dcbd8a8f-eee7-4df5-9c54-f98911b5bb70": {
			"classDefinition": "com.sap.bpm.wfs.ServiceTask",
			"destination": "VCF_Vendor_Application_log",
			"destinationSource": "consumer",
			"path": "/odata/v4/ApplicationLogService/ApplicationLogGeneral${context.general_key}",
			"httpMethod": "PATCH",
			"requestVariable": "${context.general_input}",
			"responseVariable": "${context.general_output}",
			"id": "servicetask10",
			"name": "General service"
		},
		"416d148a-5fb4-4788-95a2-60387070c39a": {
			"classDefinition": "com.sap.bpm.wfs.ReferencedSubflow",
			"workflowReference": "/workflows/wf_z007_usertask.workflow",
			"definitionId": "wf_z007_usertask",
			"multiInstanceLoopCharacteristics": {
				"type": "parallel",
				"collection": "${context.workflow1_mro}",
				"completionCondition": "${context.workflow1_mro[loop.counter].decision== \"false\"}"
			},
			"inParameters": [{
				"sourceExpression": "${context.mro}",
				"targetVariable": "${context.mro}"
			}, {
				"sourceExpression": "${context.preno}",
				"targetVariable": "${context.preno}"
			}, {
				"sourceExpression": "${context.purchase}",
				"targetVariable": "${context.purchase}"
			}, {
				"sourceExpression": "${context.attach}",
				"targetVariable": "${context.attach}"
			}, {
				"sourceExpression": "${context.company}",
				"targetVariable": "${context.company}"
			}, {
				"sourceExpression": "${context.appldate}",
				"targetVariable": "${context.appldate}"
			}, {
				"sourceExpression": "${context.purchase_length}",
				"targetVariable": "${context.purchase_length}"
			}, {
				"sourceExpression": "${context.general}",
				"targetVariable": "${context.general}"
			}, {
				"sourceExpression": "${context.company_length}",
				"targetVariable": "${context.company_length}"
			}, {
				"sourceExpression": "${context.pic}",
				"targetVariable": "${context.pic}"
			}, {
				"sourceExpression": "${context.reason}",
				"targetVariable": "${context.reason}"
			}, {
				"sourceExpression": "${context.role}",
				"targetVariable": "${context.role}"
			}, {
				"sourceExpression": "${context.applicant}",
				"targetVariable": "${context.applicant}"
			}, {
				"sourceExpression": "${context.vendorid}",
				"targetVariable": "${context.vendorid}"
			}, {
				"sourceExpression": "${context.no}",
				"targetVariable": "${context.no}"
			}, {
				"sourceExpression": "${context.workflow1_mro[loop.counter].email}",
				"targetVariable": "${context.mro_email}"
			}, {
				"sourceExpression": "${context.workflow1_mro[loop.counter].name}",
				"targetVariable": "${context.mro_name}"
			}, {
				"sourceExpression": "${context.workflow1_mro[loop.counter].employee_id}",
				"targetVariable": "${context.mro_employee_id}"
			}],
			"outParameters": [{
				"sourceExpression": "${context.mro_reason}",
				"targetVariable": "${context.workflow1_mro[loop.counter].reason}"
			}, {
				"sourceExpression": "${context.mro_approved}",
				"targetVariable": "${context.mro_approved}"
			}, {
				"sourceExpression": "${context.preno}",
				"targetVariable": "${context.preno}"
			}, {
				"sourceExpression": "${context.purchase}",
				"targetVariable": "${context.purchase}"
			}, {
				"sourceExpression": "${context.attach}",
				"targetVariable": "${context.attach}"
			}, {
				"sourceExpression": "${context.company}",
				"targetVariable": "${context.company}"
			}, {
				"sourceExpression": "${context.appldate}",
				"targetVariable": "${context.appldate}"
			}, {
				"sourceExpression": "${context.purchase_length}",
				"targetVariable": "${context.purchase_length}"
			}, {
				"sourceExpression": "${context.general}",
				"targetVariable": "${context.general}"
			}, {
				"sourceExpression": "${context.company_length}",
				"targetVariable": "${context.company_length}"
			}, {
				"sourceExpression": "${context.pic}",
				"targetVariable": "${context.pic}"
			}, {
				"sourceExpression": "${context.reason}",
				"targetVariable": "${context.reason}"
			}, {
				"sourceExpression": "${context.role}",
				"targetVariable": "${context.role}"
			}, {
				"sourceExpression": "${context.applicant}",
				"targetVariable": "${context.applicant}"
			}, {
				"sourceExpression": "${context.vendorid}",
				"targetVariable": "${context.vendorid}"
			}, {
				"sourceExpression": "${context.no}",
				"targetVariable": "${context.no}"
			}, {
				"sourceExpression": "${context.approved}",
				"targetVariable": "${context.workflow1_mro[loop.counter].decision}"
			}],
			"id": "referencedsubflow2",
			"name": "Application for Vendor Code Form(MRO)",
			"principalPropagationRef": "11a9b5ee-17c0-4159-9bbf-454dcfdcd5c3"
		},
		"59e9cbd1-baa4-4b13-aaec-e62241c009dd": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow3",
			"name": "SequenceFlow3",
			"sourceRef": "e57b0ff1-0a95-4fe2-aaee-8e7e8318a74f",
			"targetRef": "ce471956-fc40-4c60-8050-9146f93df90b"
		},
		"c6efcbdb-74c3-4be7-b907-5ff83a4bff77": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow5",
			"name": "SequenceFlow5",
			"sourceRef": "02e5eb98-e5e2-4cd9-88f6-b8fc98cef8e2",
			"targetRef": "c1e12728-d6f8-415a-83ee-81c9a2429c51"
		},
		"487ce51c-b9bb-43a9-a116-51fd856c6220": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"condition": "${context.approved == true}",
			"id": "sequenceflow6",
			"name": "approve",
			"sourceRef": "4edcface-205d-4caa-beb3-81af7a23066d",
			"targetRef": "1647a344-4f9d-41bf-a0ca-273199e56001"
		},
		"5174a1f9-e38a-449b-b4ab-c32b61e97ff9": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow7",
			"name": "reject",
			"sourceRef": "4edcface-205d-4caa-beb3-81af7a23066d",
			"targetRef": "a751b391-32be-4389-ab13-2f353ea70664"
		},
		"28f51e6f-1c67-4505-b170-9db53a7accd0": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow9",
			"name": "SequenceFlow9",
			"sourceRef": "c1e12728-d6f8-415a-83ee-81c9a2429c51",
			"targetRef": "4edcface-205d-4caa-beb3-81af7a23066d"
		},
		"fc3c6038-3f1c-4f93-a451-84d8033615fc": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow11",
			"name": "approve",
			"sourceRef": "b5add7d0-fe41-4f7a-8efa-ecf8efc41340",
			"targetRef": "3876a36e-1803-4ec6-8b6f-3642194dc590"
		},
		"41f45834-fe4b-4763-afcf-7f44281315fa": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"condition": "${context.mro_approved== \"false\"}",
			"id": "sequenceflow12",
			"name": "reject",
			"sourceRef": "b5add7d0-fe41-4f7a-8efa-ecf8efc41340",
			"targetRef": "4ec004d0-7c1d-4d8e-8a2f-e90c6324d8ce"
		},
		"a5549d68-5e9c-4189-9393-a691a04f4995": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow13",
			"name": "SequenceFlow13",
			"sourceRef": "ee4537c0-4cdb-49a6-aac1-723697faf785",
			"targetRef": "6db96a69-3a63-43f7-89e4-f3915c3a4b4d"
		},
		"95a8228d-d0c0-4685-b002-a0e1add3b034": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow14",
			"name": "SequenceFlow14",
			"sourceRef": "6db96a69-3a63-43f7-89e4-f3915c3a4b4d",
			"targetRef": "bd671f55-7d6a-4b99-bc40-7a311fea31c5"
		},
		"5cab1523-07e5-4695-801c-6dcbdbf0025d": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow16",
			"name": "reject",
			"sourceRef": "bd671f55-7d6a-4b99-bc40-7a311fea31c5",
			"targetRef": "b87ffcc4-8b99-4ab4-87ef-c289c3b980ab"
		},
		"4be2af38-9460-4029-814a-8607edf651c9": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"condition": "${context.mro == true}",
			"id": "sequenceflow17",
			"name": "MRO",
			"sourceRef": "1647a344-4f9d-41bf-a0ca-273199e56001",
			"targetRef": "416d148a-5fb4-4788-95a2-60387070c39a"
		},
		"316a1ad3-e80d-4870-b4c1-d5dfe07b2556": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow18",
			"name": "None MRO",
			"sourceRef": "1647a344-4f9d-41bf-a0ca-273199e56001",
			"targetRef": "3876a36e-1803-4ec6-8b6f-3642194dc590"
		},
		"2f899641-ab75-4df4-b3cb-3c536efb079d": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"condition": "${context.erp_flag == true}",
			"id": "sequenceflow20",
			"name": "created",
			"sourceRef": "39ff9ee6-f3b9-4d4b-a030-54cf9cd5e9b5",
			"targetRef": "68941912-6b70-4117-a1c2-99247d119bc7"
		},
		"4ce0c818-962f-4438-8e9b-afe15770c2be": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow21",
			"name": "created failed",
			"sourceRef": "39ff9ee6-f3b9-4d4b-a030-54cf9cd5e9b5",
			"targetRef": "ee4537c0-4cdb-49a6-aac1-723697faf785"
		},
		"f6af5035-046d-4ddd-8d4d-c8c4d832c73f": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"condition": "${context.workflow2_length > 0}",
			"id": "sequenceflow22",
			"name": ">0",
			"sourceRef": "68941912-6b70-4117-a1c2-99247d119bc7",
			"targetRef": "37e55584-faed-48ac-a490-5b6df225cf7a"
		},
		"8c837f09-e938-4544-ace4-3f6cc8441b8d": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow23",
			"name": "=0",
			"sourceRef": "68941912-6b70-4117-a1c2-99247d119bc7",
			"targetRef": "6bef400e-2b19-4d0d-8f45-a620ba9a4e17"
		},
		"c7c38c0c-a8af-4ddd-8f48-8190362e387f": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow24",
			"name": "SequenceFlow24",
			"sourceRef": "39eb65af-b2ee-4fc1-93bc-371e295a9cda",
			"targetRef": "5b3bed6c-5129-47ea-a26f-30a817187831"
		},
		"d27a48b3-a8cd-4116-b949-c3cfa67dff40": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow25",
			"name": "SequenceFlow25",
			"sourceRef": "37e55584-faed-48ac-a490-5b6df225cf7a",
			"targetRef": "39eb65af-b2ee-4fc1-93bc-371e295a9cda"
		},
		"3c0d1c11-dc5e-41bd-bcaa-b3cb606739a4": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow26",
			"name": "SequenceFlow26",
			"sourceRef": "5b3bed6c-5129-47ea-a26f-30a817187831",
			"targetRef": "b70dac8d-4a75-4b35-975f-b82c457a5a6e"
		},
		"d9c43d98-1678-491d-9ea1-e0bbf1fe49b0": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"condition": "${context.workflow2_length == 0}",
			"id": "sequenceflow28",
			"name": "end",
			"sourceRef": "1bab0a79-dcb1-4a0d-9455-82f39d1740f7",
			"targetRef": "2798f4e7-bc42-4fad-a248-159095a2f40a"
		},
		"56d58e6e-4776-4075-80de-e1b610e56f29": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow29",
			"name": "call sub workflow",
			"sourceRef": "1bab0a79-dcb1-4a0d-9455-82f39d1740f7",
			"targetRef": "37e55584-faed-48ac-a490-5b6df225cf7a"
		},
		"6ab79f0c-6f91-4005-b6ec-a69ae0f83f2d": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow32",
			"name": "SequenceFlow32",
			"sourceRef": "b70dac8d-4a75-4b35-975f-b82c457a5a6e",
			"targetRef": "dcbd8a8f-eee7-4df5-9c54-f98911b5bb70"
		},
		"983b2103-7f26-4810-90c2-5358809316de": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow35",
			"name": "SequenceFlow35",
			"sourceRef": "11a9b5ee-17c0-4159-9bbf-454dcfdcd5c3",
			"targetRef": "a26cb84d-d2d1-4f6e-9f50-26cc707cd8df"
		},
		"050bb5a2-825e-4d19-851b-be9735863da1": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"condition": "${context.approved == true}",
			"id": "sequenceflow36",
			"name": "SequenceFlow36",
			"sourceRef": "bd671f55-7d6a-4b99-bc40-7a311fea31c5",
			"targetRef": "3c648857-5ceb-4d60-a5c9-e4818b90dc31"
		},
		"3755b090-3267-48b9-b6aa-55fef70b312a": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow37",
			"name": "SequenceFlow37",
			"sourceRef": "3c648857-5ceb-4d60-a5c9-e4818b90dc31",
			"targetRef": "dbb3ff87-4ec2-410a-9c1c-f588e0cda1f3"
		},
		"dd23ed62-4671-4e2e-8484-e2a5778ebb87": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow38",
			"name": "SequenceFlow38",
			"sourceRef": "dbb3ff87-4ec2-410a-9c1c-f588e0cda1f3",
			"targetRef": "e91eaa37-85ec-4f93-aaed-7a7bb6d0194b"
		},
		"2dee1e62-bf0e-4a3c-b128-fa0daea4347f": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow39",
			"name": "SequenceFlow39",
			"sourceRef": "40a6913c-e571-457a-b014-a16373cc5d02",
			"targetRef": "39ff9ee6-f3b9-4d4b-a030-54cf9cd5e9b5"
		},
		"401c19ed-24d5-42ab-8cbd-008af9c7f346": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow43",
			"name": "SequenceFlow43",
			"sourceRef": "e91eaa37-85ec-4f93-aaed-7a7bb6d0194b",
			"targetRef": "40a6913c-e571-457a-b014-a16373cc5d02"
		},
		"7277a0e0-d563-40ab-a052-3af19e9ed5e8": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow45",
			"name": "SequenceFlow45",
			"sourceRef": "ce471956-fc40-4c60-8050-9146f93df90b",
			"targetRef": "02e5eb98-e5e2-4cd9-88f6-b8fc98cef8e2"
		},
		"4d568e9c-87e6-400a-a79f-5e34adab0c87": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow50",
			"name": "SequenceFlow50",
			"sourceRef": "3876a36e-1803-4ec6-8b6f-3642194dc590",
			"targetRef": "ee4537c0-4cdb-49a6-aac1-723697faf785"
		},
		"62653910-bd79-4d50-9383-088db4501b83": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow56",
			"name": "SequenceFlow56",
			"sourceRef": "a26cb84d-d2d1-4f6e-9f50-26cc707cd8df",
			"targetRef": "7eb37dee-e558-4fd6-8ab2-0e07115865a0"
		},
		"9a29462a-43ce-41f8-aa4f-747d8fb0ba8d": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow57",
			"name": "SequenceFlow57",
			"sourceRef": "7eb37dee-e558-4fd6-8ab2-0e07115865a0",
			"targetRef": "e57b0ff1-0a95-4fe2-aaee-8e7e8318a74f"
		},
		"8ea517c9-a129-481b-bd9a-f9c836921f3c": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow60",
			"name": "SequenceFlow60",
			"sourceRef": "dcbd8a8f-eee7-4df5-9c54-f98911b5bb70",
			"targetRef": "1bab0a79-dcb1-4a0d-9455-82f39d1740f7"
		},
		"779df3cb-4d98-4204-9e76-f9a86a54ce08": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow61",
			"name": "SequenceFlow61",
			"sourceRef": "416d148a-5fb4-4788-95a2-60387070c39a",
			"targetRef": "b5add7d0-fe41-4f7a-8efa-ecf8efc41340"
		},
		"42fa7a2d-c526-4a02-b3ba-49b5168ba644": {
			"classDefinition": "com.sap.bpm.wfs.ui.Diagram",
			"symbols": {
				"df898b52-91e1-4778-baad-2ad9a261d30e": {},
				"53e54950-7757-4161-82c9-afa7e86cff2c": {},
				"7d733716-8410-4050-a597-f820fbdfe154": {},
				"3e340a4c-1785-4902-b7d2-d014f54112c0": {},
				"7e610b3e-ebd8-4446-aeea-35b842b505f3": {},
				"ecc9c1a2-cd74-479c-b3a2-c4299ccefa65": {},
				"75d650e3-6c70-4778-854b-6682d51f60e7": {},
				"b6a48e41-9519-46dc-8e4f-51e4a0773420": {},
				"12150ea7-1a82-4984-b324-c164d3ec04ea": {},
				"f0ded94c-8c95-4977-9785-542cfda0300b": {},
				"8e2e734e-4c4c-41a1-a12b-e61a7fb48a01": {},
				"3af4d432-835d-49ee-81b9-92119f314d67": {},
				"b2c9efe7-efdd-4b6b-8696-a09f50d2ce42": {},
				"00fa0eea-34f7-407c-857d-92ff0b0d706d": {},
				"77cabbf2-ba01-4046-a410-6e85657e4ee2": {},
				"07ae2903-35a4-4474-8f89-10f7d1cf469c": {},
				"7794928d-d6cb-4fcb-bd91-bd7b9487f1df": {},
				"6e7c2ad5-2a4e-49f2-b0b1-b38f93936fda": {},
				"74301b8f-42af-4eb8-a94f-d458bbfb6e6d": {},
				"0f91c3f2-8e65-4e80-a1f9-309293e68a7a": {},
				"04abddb1-708a-405f-b7b6-9adb28bb20e8": {},
				"1b1aada2-fc02-45c5-b229-33f8ea2995bd": {},
				"28c75238-7848-4ff6-aca1-b0cce54817a4": {},
				"1864c0d9-d063-4c4d-a010-eb7c89a7911d": {},
				"66baf197-1479-40f8-b696-a5a59fa0a1ed": {},
				"f28b4100-60f4-4c4b-9afc-d9cf5922cb88": {},
				"f2d6e9f8-be12-4f85-ad9f-67591cf1311a": {},
				"ed9d8aa9-658f-4b25-992e-1406be724269": {},
				"0ab9afdf-ba8c-4e94-8142-2d000f94baa3": {},
				"6ffee4c0-6048-4ad1-966f-cbcfa8f01c39": {},
				"0c04291e-89dd-4992-91fb-b69cf797d3f7": {},
				"c02af149-b836-4dc0-a197-0c7a4fc5db0e": {},
				"054dccce-54a8-4b32-80f1-832eb6fbad96": {},
				"7b02759b-4857-41cf-8887-5352199941ea": {},
				"8cc5d0a7-62b2-47cf-9863-5f6f9f1f3767": {},
				"5d447eae-265d-4354-bb97-3b88fad3b186": {},
				"d2e4300b-84db-4133-a8e6-9948de57cbdf": {},
				"ff6bc668-28dd-45e2-8c96-28c618501671": {},
				"dca1d7f1-6b90-45bd-bf66-a43ca7efc5a9": {},
				"b931c581-9c6c-49d6-af42-fed9f86bf509": {},
				"0e525cee-94e8-4f75-939e-9940a14fd34b": {},
				"f978c898-2f76-4e8b-954a-1a5cd701a757": {},
				"9f355da5-5771-4135-9141-d18858fc98d2": {},
				"ba8d289f-62b3-4010-95d0-6266aa316002": {},
				"f6f3c489-9392-4ee4-a303-4f24d5ffbfbb": {},
				"0bd2b871-e234-45b8-9fec-e6c62a63dca2": {},
				"57f05581-bec4-4233-9434-cd580d6d10cd": {},
				"352048dd-02d1-484f-af47-2fe14ae33b6b": {},
				"daa85109-5d42-44e7-8ef8-ceb1bb9aed67": {},
				"9d20dc2f-9b75-49f5-bfb3-ab065d63fcb0": {},
				"2bb7d081-b2d7-484e-8713-6ce35de44ad0": {},
				"db40ad47-727a-41d6-815d-fff5c0098e25": {},
				"13f937d8-0add-4b05-bf03-30016cfe85fa": {},
				"90d5e41c-ea4a-4ca4-9d28-2ae8f7b68918": {},
				"8831bf29-0f54-47ac-bc1f-c57c785b7221": {},
				"3849b1b1-c49b-4d6d-a6bc-fcec5bc5ad61": {},
				"4b0623fd-0dfb-431a-9bc7-79042e419261": {},
				"30b396b8-e457-4a46-aeca-5fc05efca5ed": {},
				"5f6f39a3-89d5-4c88-91a4-5f95f62a65e5": {},
				"9300e54c-f852-4709-82ec-a3e7c4430b4f": {},
				"f4c98c6b-516b-43cf-9258-7155bf291b93": {},
				"466a6fcc-700a-42d7-9fac-6e3585372506": {},
				"829596b7-c50a-43e5-9e2c-01b61758a2ee": {},
				"f1974119-8024-45f2-bc47-db06641c2fda": {},
				"a1deca40-91a2-47b9-985f-e0f65b2eef8f": {},
				"1f0961ab-03c4-4db7-88c2-0b380ddcaf24": {}
			}
		},
		"cc88bc85-6ff4-4ea1-a229-572ea27be3c6": {
			"classDefinition": "com.sap.bpm.wfs.TerminateEventDefinition",
			"id": "terminateeventdefinition1"
		},
		"66d8f718-151e-4944-8732-e2fd6c046c89": {
			"classDefinition": "com.sap.bpm.wfs.TerminateEventDefinition",
			"id": "terminateeventdefinition2"
		},
		"fb30b137-aaf4-4520-9749-ae90d4094480": {
			"classDefinition": "com.sap.bpm.wfs.TerminateEventDefinition",
			"id": "terminateeventdefinition3"
		},
		"e505fe4b-cc38-4b8c-8951-6f29bdb28df3": {
			"classDefinition": "com.sap.bpm.wfs.TerminateEventDefinition",
			"id": "terminateeventdefinition4"
		},
		"2c1763cd-e863-46e2-84b2-d5ea1ec463de": {
			"classDefinition": "com.sap.bpm.wfs.MessageEventDefinition",
			"responseVariable": "${context.erp_message}",
			"id": "messageeventdefinition4",
			"messageRef": "9365508a-d5b0-4c5f-8a6d-cb05949dae7a"
		},
		"df898b52-91e1-4778-baad-2ad9a261d30e": {
			"classDefinition": "com.sap.bpm.wfs.ui.StartEventSymbol",
			"x": 106.49999970197678,
			"y": -249,
			"width": 32,
			"height": 32,
			"object": "11a9b5ee-17c0-4159-9bbf-454dcfdcd5c3"
		},
		"53e54950-7757-4161-82c9-afa7e86cff2c": {
			"classDefinition": "com.sap.bpm.wfs.ui.EndEventSymbol",
			"x": 108.99999970197678,
			"y": 2432.999988079071,
			"width": 35,
			"height": 35,
			"object": "2798f4e7-bc42-4fad-a248-159095a2f40a"
		},
		"7d733716-8410-4050-a597-f820fbdfe154": {
			"classDefinition": "com.sap.bpm.wfs.ui.ScriptTaskSymbol",
			"x": 72.49999970197678,
			"y": 123,
			"width": 100,
			"height": 60,
			"object": "e57b0ff1-0a95-4fe2-aaee-8e7e8318a74f"
		},
		"3e340a4c-1785-4902-b7d2-d014f54112c0": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "122.24999985098839,153 122.24999985098839,243",
			"sourceSymbol": "7d733716-8410-4050-a597-f820fbdfe154",
			"targetSymbol": "8831bf29-0f54-47ac-bc1f-c57c785b7221",
			"object": "59e9cbd1-baa4-4b13-aaec-e62241c009dd"
		},
		"7e610b3e-ebd8-4446-aeea-35b842b505f3": {
			"classDefinition": "com.sap.bpm.wfs.ui.UserTaskSymbol",
			"x": 72.49999970197678,
			"y": 314,
			"width": 100,
			"height": 60,
			"object": "02e5eb98-e5e2-4cd9-88f6-b8fc98cef8e2"
		},
		"ecc9c1a2-cd74-479c-b3a2-c4299ccefa65": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "122.49999970197678,374 122.49999970197678,424",
			"sourceSymbol": "7e610b3e-ebd8-4446-aeea-35b842b505f3",
			"targetSymbol": "8e2e734e-4c4c-41a1-a12b-e61a7fb48a01",
			"object": "c6efcbdb-74c3-4be7-b907-5ff83a4bff77"
		},
		"75d650e3-6c70-4778-854b-6682d51f60e7": {
			"classDefinition": "com.sap.bpm.wfs.ui.ExclusiveGatewaySymbol",
			"x": 101.49999970197678,
			"y": 534,
			"object": "4edcface-205d-4caa-beb3-81af7a23066d"
		},
		"b6a48e41-9519-46dc-8e4f-51e4a0773420": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "122.49999970197678,576 122.49999970197678,610.9999994039536 79.99999970197678,610.9999994039536 79.99999970197678,645.9999988079071",
			"sourceSymbol": "75d650e3-6c70-4778-854b-6682d51f60e7",
			"targetSymbol": "1864c0d9-d063-4c4d-a010-eb7c89a7911d",
			"object": "487ce51c-b9bb-43a9-a116-51fd856c6220"
		},
		"12150ea7-1a82-4984-b324-c164d3ec04ea": {
			"classDefinition": "com.sap.bpm.wfs.ui.EndEventSymbol",
			"x": 150.99999970197678,
			"y": 645.9999988079071,
			"width": 35,
			"height": 35,
			"object": "a751b391-32be-4389-ab13-2f353ea70664"
		},
		"f0ded94c-8c95-4977-9785-542cfda0300b": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "122.49999970197678,576 122.49999970197678,610.9999994039536 168.49999970197678,610.9999994039536 168.49999970197678,645.9999988079071",
			"sourceSymbol": "75d650e3-6c70-4778-854b-6682d51f60e7",
			"targetSymbol": "12150ea7-1a82-4984-b324-c164d3ec04ea",
			"object": "5174a1f9-e38a-449b-b4ab-c32b61e97ff9"
		},
		"8e2e734e-4c4c-41a1-a12b-e61a7fb48a01": {
			"classDefinition": "com.sap.bpm.wfs.ui.ScriptTaskSymbol",
			"x": 72.49999970197678,
			"y": 424,
			"width": 100,
			"height": 60,
			"object": "c1e12728-d6f8-415a-83ee-81c9a2429c51"
		},
		"3af4d432-835d-49ee-81b9-92119f314d67": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "122.49999970197678,484 122.49999970197678,534",
			"sourceSymbol": "8e2e734e-4c4c-41a1-a12b-e61a7fb48a01",
			"targetSymbol": "75d650e3-6c70-4778-854b-6682d51f60e7",
			"object": "28f51e6f-1c67-4505-b170-9db53a7accd0"
		},
		"b2c9efe7-efdd-4b6b-8696-a09f50d2ce42": {
			"classDefinition": "com.sap.bpm.wfs.ui.ExclusiveGatewaySymbol",
			"x": 83.5,
			"y": 997.9999964237213,
			"object": "b5add7d0-fe41-4f7a-8efa-ecf8efc41340"
		},
		"00fa0eea-34f7-407c-857d-92ff0b0d706d": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "104.5,1018.9999964237213 170.5,1019 170.5,1080",
			"sourceSymbol": "b2c9efe7-efdd-4b6b-8696-a09f50d2ce42",
			"targetSymbol": "4b0623fd-0dfb-431a-9bc7-79042e419261",
			"object": "fc3c6038-3f1c-4f93-a451-84d8033615fc"
		},
		"77cabbf2-ba01-4046-a410-6e85657e4ee2": {
			"classDefinition": "com.sap.bpm.wfs.ui.EndEventSymbol",
			"x": 29.999999701976776,
			"y": 1109.9999952316284,
			"width": 35,
			"height": 35,
			"object": "4ec004d0-7c1d-4d8e-8a2f-e90c6324d8ce"
		},
		"07ae2903-35a4-4474-8f89-10f7d1cf469c": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "104.5,1039.9999964237213 104.5,1074.9999958276749 47.499999701976776,1074.9999958276749 47.499999701976776,1109.9999952316284",
			"sourceSymbol": "b2c9efe7-efdd-4b6b-8696-a09f50d2ce42",
			"targetSymbol": "77cabbf2-ba01-4046-a410-6e85657e4ee2",
			"object": "41f45834-fe4b-4763-afcf-7f44281315fa"
		},
		"7794928d-d6cb-4fcb-bd91-bd7b9487f1df": {
			"classDefinition": "com.sap.bpm.wfs.ui.UserTaskSymbol",
			"x": 114.99999970197678,
			"y": 1126.9999952316284,
			"width": 100,
			"height": 60,
			"object": "ee4537c0-4cdb-49a6-aac1-723697faf785"
		},
		"6e7c2ad5-2a4e-49f2-b0b1-b38f93936fda": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "164.99999970197678,1186.9999952316284 165,1213.75 104.5,1213.75 104.5,1239.9999940395355",
			"sourceSymbol": "7794928d-d6cb-4fcb-bd91-bd7b9487f1df",
			"targetSymbol": "74301b8f-42af-4eb8-a94f-d458bbfb6e6d",
			"object": "a5549d68-5e9c-4189-9393-a691a04f4995"
		},
		"74301b8f-42af-4eb8-a94f-d458bbfb6e6d": {
			"classDefinition": "com.sap.bpm.wfs.ui.ScriptTaskSymbol",
			"x": 54.5,
			"y": 1239.9999940395355,
			"width": 100,
			"height": 60,
			"object": "6db96a69-3a63-43f7-89e4-f3915c3a4b4d"
		},
		"0f91c3f2-8e65-4e80-a1f9-309293e68a7a": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "104.5,1299.9999940395355 104.5,1369.9999928474426",
			"sourceSymbol": "74301b8f-42af-4eb8-a94f-d458bbfb6e6d",
			"targetSymbol": "04abddb1-708a-405f-b7b6-9adb28bb20e8",
			"object": "95a8228d-d0c0-4685-b002-a0e1add3b034"
		},
		"04abddb1-708a-405f-b7b6-9adb28bb20e8": {
			"classDefinition": "com.sap.bpm.wfs.ui.ExclusiveGatewaySymbol",
			"x": 83.5,
			"y": 1369.9999928474426,
			"object": "bd671f55-7d6a-4b99-bc40-7a311fea31c5"
		},
		"1b1aada2-fc02-45c5-b229-33f8ea2995bd": {
			"classDefinition": "com.sap.bpm.wfs.ui.EndEventSymbol",
			"x": 83,
			"y": 1480.9999916553497,
			"width": 35,
			"height": 35,
			"object": "b87ffcc4-8b99-4ab4-87ef-c289c3b980ab"
		},
		"28c75238-7848-4ff6-aca1-b0cce54817a4": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "102.5,1411.9999928474426 102.5,1480.9999916553497",
			"sourceSymbol": "04abddb1-708a-405f-b7b6-9adb28bb20e8",
			"targetSymbol": "1b1aada2-fc02-45c5-b229-33f8ea2995bd",
			"object": "5cab1523-07e5-4695-801c-6dcbdbf0025d"
		},
		"1864c0d9-d063-4c4d-a010-eb7c89a7911d": {
			"classDefinition": "com.sap.bpm.wfs.ui.ExclusiveGatewaySymbol",
			"x": 58.999999701976776,
			"y": 645.9999988079071,
			"object": "1647a344-4f9d-41bf-a0ca-273199e56001"
		},
		"66baf197-1479-40f8-b696-a5a59fa0a1ed": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "84.49999985098839,666.9999988079071 84.49999985098839,854",
			"sourceSymbol": "1864c0d9-d063-4c4d-a010-eb7c89a7911d",
			"targetSymbol": "a1deca40-91a2-47b9-985f-e0f65b2eef8f",
			"object": "4be2af38-9460-4029-814a-8607edf651c9"
		},
		"f28b4100-60f4-4c4b-9afc-d9cf5922cb88": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "79.99999967074882,664.9999923706055 271.9999997659734,664.999992060722 272.0000000952246,868.9999996901165 271.5,869 271.5,1080 220.5,1080",
			"sourceSymbol": "1864c0d9-d063-4c4d-a010-eb7c89a7911d",
			"targetSymbol": "4b0623fd-0dfb-431a-9bc7-79042e419261",
			"object": "316a1ad3-e80d-4870-b4c1-d5dfe07b2556"
		},
		"f2d6e9f8-be12-4f85-ad9f-67591cf1311a": {
			"classDefinition": "com.sap.bpm.wfs.ui.ScriptTaskSymbol",
			"x": -76,
			"y": 1602.9999916553497,
			"width": 100,
			"height": 60,
			"object": "40a6913c-e571-457a-b014-a16373cc5d02"
		},
		"ed9d8aa9-658f-4b25-992e-1406be724269": {
			"classDefinition": "com.sap.bpm.wfs.ui.ExclusiveGatewaySymbol",
			"x": 101.49999970197678,
			"y": 1611.9999904632568,
			"object": "39ff9ee6-f3b9-4d4b-a030-54cf9cd5e9b5"
		},
		"0ab9afdf-ba8c-4e94-8142-2d000f94baa3": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "122,1653.9999904632568 122,1703.9999904632568",
			"sourceSymbol": "ed9d8aa9-658f-4b25-992e-1406be724269",
			"targetSymbol": "0c04291e-89dd-4992-91fb-b69cf797d3f7",
			"object": "2f899641-ab75-4df4-b3cb-3c536efb079d"
		},
		"6ffee4c0-6048-4ad1-966f-cbcfa8f01c39": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "122.49999966972013,1635 164.99999985694885,1635.0000000596046 165.00000018722872,1399.5000000596046 165,1399.5 164.99999970197678,1186.9999952316284",
			"sourceSymbol": "ed9d8aa9-658f-4b25-992e-1406be724269",
			"targetSymbol": "7794928d-d6cb-4fcb-bd91-bd7b9487f1df",
			"object": "4ce0c818-962f-4438-8e9b-afe15770c2be"
		},
		"0c04291e-89dd-4992-91fb-b69cf797d3f7": {
			"classDefinition": "com.sap.bpm.wfs.ui.ExclusiveGatewaySymbol",
			"x": 101.49999970197678,
			"y": 1703.9999904632568,
			"object": "68941912-6b70-4117-a1c2-99247d119bc7"
		},
		"c02af149-b836-4dc0-a197-0c7a4fc5db0e": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "122.49999970197678,1723 79.99999970197678,1723 79.99999970197678,1815.999989271164",
			"sourceSymbol": "0c04291e-89dd-4992-91fb-b69cf797d3f7",
			"targetSymbol": "d2e4300b-84db-4133-a8e6-9948de57cbdf",
			"object": "f6af5035-046d-4ddd-8d4d-c8c4d832c73f"
		},
		"054dccce-54a8-4b32-80f1-832eb6fbad96": {
			"classDefinition": "com.sap.bpm.wfs.ui.EndEventSymbol",
			"x": 179.99999970197678,
			"y": 1815.999989271164,
			"width": 35,
			"height": 35,
			"object": "6bef400e-2b19-4d0d-8f45-a620ba9a4e17"
		},
		"7b02759b-4857-41cf-8887-5352199941ea": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "122.49999970197678,1745.9999904632568 122.49999970197678,1780.9999898672104 197.49999970197678,1780.9999898672104 197.49999970197678,1815.999989271164",
			"sourceSymbol": "0c04291e-89dd-4992-91fb-b69cf797d3f7",
			"targetSymbol": "054dccce-54a8-4b32-80f1-832eb6fbad96",
			"object": "8c837f09-e938-4544-ace4-3f6cc8441b8d"
		},
		"8cc5d0a7-62b2-47cf-9863-5f6f9f1f3767": {
			"classDefinition": "com.sap.bpm.wfs.ui.ServiceTaskSymbol",
			"x": 72.49999970197678,
			"y": 1945.999988079071,
			"width": 100,
			"height": 60,
			"object": "39eb65af-b2ee-4fc1-93bc-371e295a9cda"
		},
		"5d447eae-265d-4354-bb97-3b88fad3b186": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "122.49999970197678,2005.999988079071 122.49999970197678,2055.999988079071",
			"sourceSymbol": "8cc5d0a7-62b2-47cf-9863-5f6f9f1f3767",
			"targetSymbol": "dca1d7f1-6b90-45bd-bf66-a43ca7efc5a9",
			"object": "c7c38c0c-a8af-4ddd-8f48-8190362e387f"
		},
		"d2e4300b-84db-4133-a8e6-9948de57cbdf": {
			"classDefinition": "com.sap.bpm.wfs.ui.ScriptTaskSymbol",
			"x": 29.999999701976776,
			"y": 1815.999989271164,
			"width": 100,
			"height": 60,
			"object": "37e55584-faed-48ac-a490-5b6df225cf7a"
		},
		"ff6bc668-28dd-45e2-8c96-28c618501671": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "79.99999970197678,1875.999989271164 79.99999970197678,1910.9999886751175 122.49999970197678,1910.9999886751175 122.49999970197678,1945.999988079071",
			"sourceSymbol": "d2e4300b-84db-4133-a8e6-9948de57cbdf",
			"targetSymbol": "8cc5d0a7-62b2-47cf-9863-5f6f9f1f3767",
			"object": "d27a48b3-a8cd-4116-b949-c3cfa67dff40"
		},
		"dca1d7f1-6b90-45bd-bf66-a43ca7efc5a9": {
			"classDefinition": "com.sap.bpm.wfs.ui.ScriptTaskSymbol",
			"x": 72.49999970197678,
			"y": 2055.999988079071,
			"width": 100,
			"height": 60,
			"object": "5b3bed6c-5129-47ea-a26f-30a817187831"
		},
		"b931c581-9c6c-49d6-af42-fed9f86bf509": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "122.24999985098839,2085.999988079071 122.24999985098839,2172",
			"sourceSymbol": "dca1d7f1-6b90-45bd-bf66-a43ca7efc5a9",
			"targetSymbol": "ba8d289f-62b3-4010-95d0-6266aa316002",
			"object": "3c0d1c11-dc5e-41bd-bcaa-b3cb606739a4"
		},
		"0e525cee-94e8-4f75-939e-9940a14fd34b": {
			"classDefinition": "com.sap.bpm.wfs.ui.ExclusiveGatewaySymbol",
			"x": 106,
			"y": 2320,
			"object": "1bab0a79-dcb1-4a0d-9455-82f39d1740f7"
		},
		"f978c898-2f76-4e8b-954a-1a5cd701a757": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "126.74999985098839,2341 126.74999985098839,2450.499988079071",
			"sourceSymbol": "0e525cee-94e8-4f75-939e-9940a14fd34b",
			"targetSymbol": "53e54950-7757-4161-82c9-afa7e86cff2c",
			"object": "d9c43d98-1678-491d-9ea1-e0bbf1fe49b0"
		},
		"9f355da5-5771-4135-9141-d18858fc98d2": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "127,2343 -20.5,2343 -20.5,1839 32,1839",
			"sourceSymbol": "0e525cee-94e8-4f75-939e-9940a14fd34b",
			"targetSymbol": "d2e4300b-84db-4133-a8e6-9948de57cbdf",
			"object": "56d58e6e-4776-4075-80de-e1b610e56f29"
		},
		"ba8d289f-62b3-4010-95d0-6266aa316002": {
			"classDefinition": "com.sap.bpm.wfs.ui.ScriptTaskSymbol",
			"x": 72,
			"y": 2142,
			"width": 100,
			"height": 60,
			"object": "b70dac8d-4a75-4b35-975f-b82c457a5a6e"
		},
		"f6f3c489-9392-4ee4-a303-4f24d5ffbfbb": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "122,2172 122,2268",
			"sourceSymbol": "ba8d289f-62b3-4010-95d0-6266aa316002",
			"targetSymbol": "829596b7-c50a-43e5-9e2c-01b61758a2ee",
			"object": "6ab79f0c-6f91-4005-b6ec-a69ae0f83f2d"
		},
		"0bd2b871-e234-45b8-9fec-e6c62a63dca2": {
			"classDefinition": "com.sap.bpm.wfs.ui.ServiceTaskSymbol",
			"x": -76,
			"y": 1451,
			"width": 100,
			"height": 60,
			"object": "dbb3ff87-4ec2-410a-9c1c-f588e0cda1f3"
		},
		"57f05581-bec4-4233-9434-cd580d6d10cd": {
			"classDefinition": "com.sap.bpm.wfs.ui.ScriptTaskSymbol",
			"x": -76,
			"y": 1361,
			"width": 100,
			"height": 60,
			"object": "3c648857-5ceb-4d60-a5c9-e4818b90dc31"
		},
		"352048dd-02d1-484f-af47-2fe14ae33b6b": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "122.24999985098839,-233 122.24999985098839,-35",
			"sourceSymbol": "df898b52-91e1-4778-baad-2ad9a261d30e",
			"targetSymbol": "5f6f39a3-89d5-4c88-91a4-5f95f62a65e5",
			"object": "983b2103-7f26-4810-90c2-5358809316de"
		},
		"daa85109-5d42-44e7-8ef8-ceb1bb9aed67": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "104.5,1390.9999928474426 67.0000076823761,1390.9999961588112 67.00000768237655,1390.9999986886978 23.500000000000455,1391.0000025298866",
			"sourceSymbol": "04abddb1-708a-405f-b7b6-9adb28bb20e8",
			"targetSymbol": "57f05581-bec4-4233-9434-cd580d6d10cd",
			"object": "050bb5a2-825e-4d19-851b-be9735863da1"
		},
		"9d20dc2f-9b75-49f5-bfb3-ab065d63fcb0": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "-26,1391 -26,1451.5",
			"sourceSymbol": "57f05581-bec4-4233-9434-cd580d6d10cd",
			"targetSymbol": "0bd2b871-e234-45b8-9fec-e6c62a63dca2",
			"object": "3755b090-3267-48b9-b6aa-55fef70b312a"
		},
		"2bb7d081-b2d7-484e-8713-6ce35de44ad0": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "-26,1481 -26,1556",
			"sourceSymbol": "0bd2b871-e234-45b8-9fec-e6c62a63dca2",
			"targetSymbol": "13f937d8-0add-4b05-bf03-30016cfe85fa",
			"object": "dd23ed62-4671-4e2e-8484-e2a5778ebb87"
		},
		"db40ad47-727a-41d6-815d-fff5c0098e25": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "-26,1632.9999916553497 101.99999970197678,1632.9999904632568",
			"sourceSymbol": "f2d6e9f8-be12-4f85-ad9f-67591cf1311a",
			"targetSymbol": "ed9d8aa9-658f-4b25-992e-1406be724269",
			"object": "2dee1e62-bf0e-4a3c-b128-fa0daea4347f"
		},
		"13f937d8-0add-4b05-bf03-30016cfe85fa": {
			"classDefinition": "com.sap.bpm.wfs.ui.IntermediateCatchEventSymbol",
			"x": -42,
			"y": 1540,
			"width": 32,
			"height": 32,
			"object": "e91eaa37-85ec-4f93-aaed-7a7bb6d0194b"
		},
		"90d5e41c-ea4a-4ca4-9d28-2ae8f7b68918": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "-26,1556 -26,1632.9999916553497",
			"sourceSymbol": "13f937d8-0add-4b05-bf03-30016cfe85fa",
			"targetSymbol": "f2d6e9f8-be12-4f85-ad9f-67591cf1311a",
			"object": "401c19ed-24d5-42ab-8cbd-008af9c7f346"
		},
		"8831bf29-0f54-47ac-bc1f-c57c785b7221": {
			"classDefinition": "com.sap.bpm.wfs.ui.MailTaskSymbol",
			"x": 72,
			"y": 213,
			"width": 100,
			"height": 60,
			"object": "ce471956-fc40-4c60-8050-9146f93df90b"
		},
		"3849b1b1-c49b-4d6d-a6bc-fcec5bc5ad61": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "122.24999985098839,243 122.24999985098839,344",
			"sourceSymbol": "8831bf29-0f54-47ac-bc1f-c57c785b7221",
			"targetSymbol": "7e610b3e-ebd8-4446-aeea-35b842b505f3",
			"object": "7277a0e0-d563-40ab-a052-3af19e9ed5e8"
		},
		"4b0623fd-0dfb-431a-9bc7-79042e419261": {
			"classDefinition": "com.sap.bpm.wfs.ui.MailTaskSymbol",
			"x": 120,
			"y": 1050,
			"width": 101,
			"height": 60,
			"object": "3876a36e-1803-4ec6-8b6f-3642194dc590"
		},
		"30b396b8-e457-4a46-aeca-5fc05efca5ed": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "167.73500053584576,1080 167.73500053584576,1156.9999952316284",
			"sourceSymbol": "4b0623fd-0dfb-431a-9bc7-79042e419261",
			"targetSymbol": "7794928d-d6cb-4fcb-bd91-bd7b9487f1df",
			"object": "4d568e9c-87e6-400a-a79f-5e34adab0c87"
		},
		"5f6f39a3-89d5-4c88-91a4-5f95f62a65e5": {
			"classDefinition": "com.sap.bpm.wfs.ui.ScriptTaskSymbol",
			"x": 72,
			"y": -65,
			"width": 100,
			"height": 60,
			"object": "a26cb84d-d2d1-4f6e-9f50-26cc707cd8df"
		},
		"9300e54c-f852-4709-82ec-a3e7c4430b4f": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "122,-35 122,57",
			"sourceSymbol": "5f6f39a3-89d5-4c88-91a4-5f95f62a65e5",
			"targetSymbol": "f4c98c6b-516b-43cf-9258-7155bf291b93",
			"object": "62653910-bd79-4d50-9383-088db4501b83"
		},
		"f4c98c6b-516b-43cf-9258-7155bf291b93": {
			"classDefinition": "com.sap.bpm.wfs.ui.ServiceTaskSymbol",
			"x": 72,
			"y": 27,
			"width": 100,
			"height": 60,
			"object": "7eb37dee-e558-4fd6-8ab2-0e07115865a0"
		},
		"466a6fcc-700a-42d7-9fac-6e3585372506": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "122.24999985098839,57 122.24999985098839,153",
			"sourceSymbol": "f4c98c6b-516b-43cf-9258-7155bf291b93",
			"targetSymbol": "7d733716-8410-4050-a597-f820fbdfe154",
			"object": "9a29462a-43ce-41f8-aa4f-747d8fb0ba8d"
		},
		"829596b7-c50a-43e5-9e2c-01b61758a2ee": {
			"classDefinition": "com.sap.bpm.wfs.ui.ServiceTaskSymbol",
			"x": 72,
			"y": 2238,
			"width": 100,
			"height": 60,
			"object": "dcbd8a8f-eee7-4df5-9c54-f98911b5bb70"
		},
		"f1974119-8024-45f2-bc47-db06641c2fda": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "124.5,2268 124.5,2341",
			"sourceSymbol": "829596b7-c50a-43e5-9e2c-01b61758a2ee",
			"targetSymbol": "0e525cee-94e8-4f75-939e-9940a14fd34b",
			"object": "8ea517c9-a129-481b-bd9a-f9c836921f3c"
		},
		"a1deca40-91a2-47b9-985f-e0f65b2eef8f": {
			"classDefinition": "com.sap.bpm.wfs.ui.ReferencedSubflowSymbol",
			"x": 39,
			"y": 824,
			"width": 100,
			"height": 60,
			"object": "416d148a-5fb4-4788-95a2-60387070c39a"
		},
		"1f0961ab-03c4-4db7-88c2-0b380ddcaf24": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "89,854 89,941.75 104.5,941.75 104.5,1018.9999964237213",
			"sourceSymbol": "a1deca40-91a2-47b9-985f-e0f65b2eef8f",
			"targetSymbol": "b2c9efe7-efdd-4b6b-8696-a09f50d2ce42",
			"object": "779df3cb-4d98-4204-9e76-f9a86a54ce08"
		},
		"62d7f4ed-4063-4c44-af8b-39050bd44926": {
			"classDefinition": "com.sap.bpm.wfs.LastIDs",
			"terminateeventdefinition": 4,
			"messageeventdefinition": 4,
			"message": 3,
			"maildefinition": 8,
			"sequenceflow": 61,
			"startevent": 1,
			"intermediatemessageevent": 4,
			"endevent": 5,
			"usertask": 4,
			"servicetask": 10,
			"scripttask": 16,
			"mailtask": 7,
			"exclusivegateway": 7,
			"parallelgateway": 1,
			"referencedsubflow": 2
		},
		"9365508a-d5b0-4c5f-8a6d-cb05949dae7a": {
			"classDefinition": "com.sap.bpm.wfs.Message",
			"name": "general",
			"id": "message3"
		},
		"bcc3ebb6-9119-4f93-9063-e99a3726f82e": {
			"classDefinition": "com.sap.bpm.wfs.MailDefinition",
			"name": "maildefinition5",
			"to": "${context.workflow1_fin}",
			"subject": "There’s a new vendor application form that needs your approval.",
			"text": "Hi \nA Vendor Code Application(Doc: No: ${context.no} )\nApplicant: ${context.applicant} requires  needs your approval\nVendor Name: ${context.vendorid}\n\nPlease click the following hyper link to open document for approval.\n\nhttps://wistronprework.cockpit.workflowmanagement.cfapps.eu10.hana.ondemand.com/cp.portal/site#WorkflowTask-DisplayMyInbox?sap-ui-app-id-hint=cross.fnd.fiori.inbox&substitution=true&userSearch=false&/detail/NA/ec412890-0b47-11ed-806d-eeee0a95fab7/TaskCollection(SAP__Origin='NA',InstanceID='ec412890-0b47-11ed-806d-eeee0a95fab7')\n\nNote:Once you have processed this document, the link above will become inactive!\n\nVCF(Vendor Code Form System)",
			"id": "maildefinition5"
		},
		"b845ca33-1b7a-409f-bfea-610d0354707e": {
			"classDefinition": "com.sap.bpm.wfs.MailDefinition",
			"name": "maildefinition6",
			"to": "${context.workflow1_leader}",
			"subject": "There’s a new vendor application form that needs your approval.",
			"text": "Hi \nA Vendor Code Application(Doc: No: ${context.no} )\nApplicant: ${context.applicant} requires  needs your approval\nVendor Name: ${context.vendorid}\n\nPlease click the following hyper link to open document for approval.\n\nhttps://wistronprework.cockpit.workflowmanagement.cfapps.eu10.hana.ondemand.com/cp.portal/site#WorkflowTask-DisplayMyInbox?sap-ui-app-id-hint=cross.fnd.fiori.inbox&substitution=true&userSearch=false&/detail/NA/ec412890-0b47-11ed-806d-eeee0a95fab7/TaskCollection(SAP__Origin='NA',InstanceID='ec412890-0b47-11ed-806d-eeee0a95fab7')\n\nNote:Once you have processed this document, the link above will become inactive!\n\nVCF(Vendor Code Form System)",
			"id": "maildefinition6"
		}
	}
}