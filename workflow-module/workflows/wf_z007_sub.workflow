{
	"contents": {
		"4b297879-16c2-4ebe-b1f1-c0a7689cfbaf": {
			"classDefinition": "com.sap.bpm.wfs.Model",
			"id": "zvcf.wf_z007_sub",
			"subject": "wf_z007_sub",
			"name": "wf_z007_sub",
			"documentation": "wf_z007_sub",
			"lastIds": "62d7f4ed-4063-4c44-af8b-39050bd44926",
			"events": {
				"11a9b5ee-17c0-4159-9bbf-454dcfdcd5c3": {
					"name": "StartEvent1"
				},
				"2798f4e7-bc42-4fad-a248-159095a2f40a": {
					"name": "End"
				},
				"46338427-c891-4838-9d01-1a8f3faaaa2d": {
					"name": "End"
				},
				"b5a144ed-e454-48d1-ac29-d56b0d002d23": {
					"name": "Wait Message"
				}
			},
			"activities": {
				"63a572e8-83d1-45cc-a506-d1f60ff6efa3": {
					"name": "Manager"
				},
				"857938bb-3134-42bc-b15b-5b4cd81f0468": {
					"name": "data"
				},
				"bb6fc91e-e94a-430a-b629-d943b174113c": {
					"name": "check"
				},
				"b9923f0c-4dcb-4a94-b952-c6dd79ac0b53": {
					"name": "Site Finance"
				},
				"806ecc4e-e1ab-4b8c-99ca-e4320a4947db": {
					"name": "Gateway"
				},
				"3f77562b-a43d-45e7-93f8-21a0c96a2970": {
					"name": "Manage Gateway"
				},
				"eb3462f7-b1a3-476c-8a5a-298c181d394f": {
					"name": "Fin Reason"
				},
				"c6875fe3-1a38-41d5-91e2-52f6baf306c7": {
					"name": "ERP data"
				},
				"c7b40236-4dd1-4fea-a49b-8590e5a00868": {
					"name": "ERP"
				},
				"e2add96e-9362-4104-b558-85ac987bfd81": {
					"name": "Gateway"
				},
				"cec11a65-a601-48e3-a035-de8f6736f439": {
					"name": "ERP Result"
				},
				"8cb0f833-c772-4bc3-a97d-e9d6b304a187": {
					"name": "Reason"
				},
				"9ef63fa4-3e32-4583-8c41-dd96daa68288": {
					"name": "Mail"
				},
				"f7a38666-5271-4cba-ba27-8429ecea87b5": {
					"name": "Mail"
				},
				"6ee9e69f-3047-482d-b04d-287de6831777": {
					"name": "Company log"
				},
				"cefd449f-53b5-4986-a964-d87f70d94e0b": {
					"name": "init"
				},
				"40f112bc-c398-4e72-aacd-26b1e4ee0e95": {
					"name": "Company service"
				},
				"1d9d4ffa-f23a-4ede-b487-2a3c442dfe19": {
					"name": "Company check"
				},
				"56b5c609-08d6-49d8-9ef7-79545cabdd7d": {
					"name": "ExclusiveGateway5"
				},
				"e98ad4bb-5659-41eb-b92a-79e37b99e9a5": {
					"name": "Purchase log"
				},
				"04b8aae2-090f-46ee-b237-bc78f9d74b7d": {
					"name": "Purchase service"
				},
				"d18f4161-4e05-4aeb-becc-a9b2066b62b7": {
					"name": "Purchase check"
				},
				"abe59c16-7e58-475a-844c-88cf32261387": {
					"name": "Gateway"
				},
				"09d7bcd0-979a-4c0d-bec3-a530ca6b294c": {
					"name": "Fin flag"
				},
				"541f65d0-82b3-4fb1-8e6a-5240f6d3708e": {
					"name": "manage gateway"
				},
				"eb82be40-1748-4f7c-b57f-467349a55bc5": {
					"name": "manager"
				}
			},
			"sequenceFlows": {
				"c6b99f32-5fe6-4ab6-b60a-80fba1b9ae0f": {
					"name": "SequenceFlow1"
				},
				"193072bb-4280-4237-90d9-8c0b34baf329": {
					"name": "SequenceFlow2"
				},
				"ab130c07-c299-41c3-b3d0-179b8bac0686": {
					"name": "SequenceFlow8"
				},
				"18da8bda-9464-480d-a55f-56f364270edf": {
					"name": "SequenceFlow9"
				},
				"e4397020-c1fc-4ffd-bbe2-438e121eb34c": {
					"name": "SequenceFlow10"
				},
				"78b46b05-66e7-44cc-b390-9830007178d5": {
					"name": "check done"
				},
				"14831d15-9b06-41bc-b619-c3a2fd7eeda7": {
					"name": "approve"
				},
				"0ed30650-baac-47ce-b3f6-bfca924a7ac5": {
					"name": "rerject"
				},
				"c2d49164-ac3d-44f2-b36d-b1c4d5d92d2f": {
					"name": "SequenceFlow21"
				},
				"241ee828-3a05-41e5-9106-78fa0dd2d03c": {
					"name": "SequenceFlow23"
				},
				"438d8028-0cc4-4e18-a366-c6ab77df2066": {
					"name": "SequenceFlow24"
				},
				"9e4c563c-7cca-4345-b6ab-083a0e213d04": {
					"name": "redo"
				},
				"32baee57-85fb-46f1-8415-9bca676b9982": {
					"name": "SequenceFlow27"
				},
				"b5b79097-1915-43a5-b7ae-7beb17ec7e23": {
					"name": "SequenceFlow28"
				},
				"710cb275-d76d-4f88-a6ee-f4570489fd4f": {
					"name": "SequenceFlow29"
				},
				"5c9c5188-07bf-49af-906a-f0316ce80722": {
					"name": "End"
				},
				"f592263f-10ab-4d15-982b-c1d53fdb3a69": {
					"name": "SequenceFlow32"
				},
				"9490a990-f17f-4793-8b7f-7bb34dae22a7": {
					"name": "SequenceFlow34"
				},
				"f654fdda-45dc-4b70-9e0e-3ce928c2bf48": {
					"name": "SequenceFlow35"
				},
				"82250b9e-05a2-44d5-bf5d-b56a3612e8aa": {
					"name": "SequenceFlow36"
				},
				"918aa604-d354-4a7f-993b-d43900c286b7": {
					"name": "SequenceFlow37"
				},
				"87dd3c1b-43d9-44c8-bee1-a3632449e679": {
					"name": "SequenceFlow38"
				},
				"59de7444-50af-420e-ba74-51cca1918ecd": {
					"name": "finished"
				},
				"321c40f8-33bd-4f08-b66e-0815d38c050e": {
					"name": "redo"
				},
				"60ac5b54-1b1c-4dc2-9f4c-f61694671124": {
					"name": "SequenceFlow41"
				},
				"3bfdb23f-7386-479a-8492-6b00c6598954": {
					"name": "SequenceFlow42"
				},
				"36731c28-b5ce-4868-8bf8-c23a1010aa86": {
					"name": "SequenceFlow43"
				},
				"256abefa-57e6-4939-abdf-b0ba7fae0ba5": {
					"name": "finished"
				},
				"e80489d6-50f5-4ed2-80d8-2b8ebe6cbf8b": {
					"name": "redo"
				},
				"c2aaa7e9-ab45-4e3f-819b-a07948caa8bc": {
					"name": "redi"
				},
				"bd440fce-7441-4560-afdd-cf42a834835f": {
					"name": "SequenceFlow47"
				},
				"4563a7d3-681b-460a-961f-876d43e18a96": {
					"name": "SequenceFlow48"
				},
				"222ebf59-b472-49b8-9fc9-268441e94571": {
					"name": "SequenceFlow49"
				},
				"b1edcd1b-54dd-4dfc-86f9-54a6ea975d89": {
					"name": "SequenceFlow50"
				}
			},
			"diagrams": {
				"42fa7a2d-c526-4a02-b3ba-49b5168ba644": {}
			}
		},
		"11a9b5ee-17c0-4159-9bbf-454dcfdcd5c3": {
			"classDefinition": "com.sap.bpm.wfs.StartEvent",
			"id": "startevent1",
			"name": "StartEvent1"
		},
		"2798f4e7-bc42-4fad-a248-159095a2f40a": {
			"classDefinition": "com.sap.bpm.wfs.EndEvent",
			"id": "endevent1",
			"name": "End"
		},
		"46338427-c891-4838-9d01-1a8f3faaaa2d": {
			"classDefinition": "com.sap.bpm.wfs.EndEvent",
			"id": "endevent2",
			"name": "End",
			"eventDefinitions": {
				"b627eeab-ad22-498a-ada8-79d683ee4a7e": {}
			}
		},
		"b5a144ed-e454-48d1-ac29-d56b0d002d23": {
			"classDefinition": "com.sap.bpm.wfs.IntermediateCatchEvent",
			"id": "intermediatemessageevent2",
			"name": "Wait Message",
			"eventDefinitions": {
				"54eb74e6-291b-4f89-a6b7-f56fc9f93c43": {}
			}
		},
		"63a572e8-83d1-45cc-a506-d1f60ff6efa3": {
			"classDefinition": "com.sap.bpm.wfs.UserTask",
			"subject": "[Sub]Manager-Form",
			"priority": "MEDIUM",
			"isHiddenInLogForParticipant": false,
			"supportsForward": true,
			"userInterface": "sapui5://zvcfwf_z007_approuter.zvcfwfz007workflowuimodule/zvcfwfz007workflowuimodule",
			"recipientUsers": "${context.managers_mail}",
			"id": "usertask1",
			"name": "Manager"
		},
		"857938bb-3134-42bc-b15b-5b4cd81f0468": {
			"classDefinition": "com.sap.bpm.wfs.ScriptTask",
			"reference": "/scripts/wf_z007_sub/script_sub_workflow_perpare_data.js",
			"id": "scripttask1",
			"name": "data"
		},
		"bb6fc91e-e94a-430a-b629-d943b174113c": {
			"classDefinition": "com.sap.bpm.wfs.ScriptTask",
			"reference": "/scripts/wf_z007_sub/script_sub_workflow_check.js",
			"id": "scripttask2",
			"name": "check"
		},
		"b9923f0c-4dcb-4a94-b952-c6dd79ac0b53": {
			"classDefinition": "com.sap.bpm.wfs.UserTask",
			"subject": "[Sub]Finance-Form",
			"priority": "MEDIUM",
			"isHiddenInLogForParticipant": false,
			"supportsForward": true,
			"userInterface": "sapui5://zvcfwf_z007_approuter.zvcfwfz007workflowuimodule/zvcfwfz007workflowuimodule",
			"recipientUsers": "${context.site_fin_mail}",
			"id": "usertask2",
			"name": "Site Finance"
		},
		"806ecc4e-e1ab-4b8c-99ca-e4320a4947db": {
			"classDefinition": "com.sap.bpm.wfs.ExclusiveGateway",
			"id": "exclusivegateway2",
			"name": "Gateway",
			"default": "c2aaa7e9-ab45-4e3f-819b-a07948caa8bc"
		},
		"3f77562b-a43d-45e7-93f8-21a0c96a2970": {
			"classDefinition": "com.sap.bpm.wfs.ExclusiveGateway",
			"id": "exclusivegateway3",
			"name": "Manage Gateway",
			"default": "0ed30650-baac-47ce-b3f6-bfca924a7ac5"
		},
		"eb3462f7-b1a3-476c-8a5a-298c181d394f": {
			"classDefinition": "com.sap.bpm.wfs.ScriptTask",
			"reference": "/scripts/wf_z007_sub/script_fin_reason.js",
			"id": "scripttask7",
			"name": "Fin Reason"
		},
		"c6875fe3-1a38-41d5-91e2-52f6baf306c7": {
			"classDefinition": "com.sap.bpm.wfs.ScriptTask",
			"reference": "/scripts/wf_z007_sub/script_erp.js",
			"id": "scripttask9",
			"name": "ERP data"
		},
		"c7b40236-4dd1-4fea-a49b-8590e5a00868": {
			"classDefinition": "com.sap.bpm.wfs.ServiceTask",
			"destination": "SAP_dev",
			"destinationSource": "consumer",
			"path": "/sap/opu/odata/sap/zcomservice/ZCOMCollection?sap-client=055",
			"httpMethod": "POST",
			"xsrfPath": "/sap/opu/odata/sap/zcomservice/ZCOMCollection?sap-client=055",
			"requestVariable": "${context.erp_input}",
			"responseVariable": "${context.erp_output}",
			"id": "servicetask3",
			"name": "ERP"
		},
		"e2add96e-9362-4104-b558-85ac987bfd81": {
			"classDefinition": "com.sap.bpm.wfs.ExclusiveGateway",
			"id": "exclusivegateway4",
			"name": "Gateway",
			"default": "9e4c563c-7cca-4345-b6ab-083a0e213d04"
		},
		"cec11a65-a601-48e3-a035-de8f6736f439": {
			"classDefinition": "com.sap.bpm.wfs.ScriptTask",
			"reference": "/scripts/wf_z007_sub/script_erp_result.js",
			"id": "scripttask10",
			"name": "ERP Result"
		},
		"8cb0f833-c772-4bc3-a97d-e9d6b304a187": {
			"classDefinition": "com.sap.bpm.wfs.ScriptTask",
			"reference": "/scripts/wf_z007_sub/script_reason.js",
			"id": "scripttask5",
			"name": "Reason"
		},
		"9ef63fa4-3e32-4583-8c41-dd96daa68288": {
			"classDefinition": "com.sap.bpm.wfs.MailTask",
			"destinationSource": "consumer",
			"id": "mailtask1",
			"name": "Mail",
			"mailDefinitionRef": "f59ceb2b-b093-4781-b84b-87b93b259b2d"
		},
		"f7a38666-5271-4cba-ba27-8429ecea87b5": {
			"classDefinition": "com.sap.bpm.wfs.MailTask",
			"destinationSource": "consumer",
			"id": "mailtask2",
			"name": "Mail",
			"mailDefinitionRef": "34a584da-b829-4a46-b16e-8eb5a3d9ed7f"
		},
		"6ee9e69f-3047-482d-b04d-287de6831777": {
			"classDefinition": "com.sap.bpm.wfs.ScriptTask",
			"reference": "/scripts/wf_z007_sub/script_companylog.js",
			"id": "scripttask12",
			"name": "Company log"
		},
		"cefd449f-53b5-4986-a964-d87f70d94e0b": {
			"classDefinition": "com.sap.bpm.wfs.ScriptTask",
			"reference": "/scripts/wf_z007_sub/script_init.js",
			"id": "scripttask13",
			"name": "init"
		},
		"40f112bc-c398-4e72-aacd-26b1e4ee0e95": {
			"classDefinition": "com.sap.bpm.wfs.ServiceTask",
			"destination": "VCF_Vendor_Application_log",
			"destinationSource": "consumer",
			"path": "/odata/v4/ApplicationLogService/ApplicationLogCompany${context.company_key}",
			"httpMethod": "PATCH",
			"requestVariable": "${context.company_input}",
			"responseVariable": "${context.company_output}",
			"id": "servicetask4",
			"name": "Company service"
		},
		"1d9d4ffa-f23a-4ede-b487-2a3c442dfe19": {
			"classDefinition": "com.sap.bpm.wfs.ScriptTask",
			"reference": "/scripts/wf_z007_sub/script_companycheck.js",
			"id": "scripttask14",
			"name": "Company check"
		},
		"56b5c609-08d6-49d8-9ef7-79545cabdd7d": {
			"classDefinition": "com.sap.bpm.wfs.ExclusiveGateway",
			"id": "exclusivegateway5",
			"name": "ExclusiveGateway5",
			"default": "321c40f8-33bd-4f08-b66e-0815d38c050e"
		},
		"e98ad4bb-5659-41eb-b92a-79e37b99e9a5": {
			"classDefinition": "com.sap.bpm.wfs.ScriptTask",
			"reference": "/scripts/wf_z007_sub/script_purchaselog.js",
			"id": "scripttask15",
			"name": "Purchase log"
		},
		"04b8aae2-090f-46ee-b237-bc78f9d74b7d": {
			"classDefinition": "com.sap.bpm.wfs.ServiceTask",
			"destination": "VCF_Vendor_Application_log",
			"destinationSource": "consumer",
			"path": "/odata/v4/ApplicationLogService/ApplicationLogPurchaseOrg${context.purchase_key}",
			"httpMethod": "PATCH",
			"xsrfPath": "",
			"requestVariable": "${context.purchase_input}",
			"responseVariable": "${context.company_output}",
			"id": "servicetask5",
			"name": "Purchase service"
		},
		"d18f4161-4e05-4aeb-becc-a9b2066b62b7": {
			"classDefinition": "com.sap.bpm.wfs.ScriptTask",
			"reference": "/scripts/wf_z007_sub/script_purchasecheck.js",
			"id": "scripttask16",
			"name": "Purchase check"
		},
		"abe59c16-7e58-475a-844c-88cf32261387": {
			"classDefinition": "com.sap.bpm.wfs.ExclusiveGateway",
			"id": "exclusivegateway6",
			"name": "Gateway",
			"default": "e80489d6-50f5-4ed2-80d8-2b8ebe6cbf8b"
		},
		"09d7bcd0-979a-4c0d-bec3-a530ca6b294c": {
			"classDefinition": "com.sap.bpm.wfs.ScriptTask",
			"reference": "/scripts/wf_z007_sub/script_finflag.js",
			"id": "scripttask17",
			"name": "Fin flag"
		},
		"541f65d0-82b3-4fb1-8e6a-5240f6d3708e": {
			"classDefinition": "com.sap.bpm.wfs.ExclusiveGateway",
			"id": "exclusivegateway7",
			"name": "manage gateway",
			"default": "222ebf59-b472-49b8-9fc9-268441e94571"
		},
		"eb82be40-1748-4f7c-b57f-467349a55bc5": {
			"classDefinition": "com.sap.bpm.wfs.ScriptTask",
			"reference": "/scripts/wf_z007_sub/script_manage.js",
			"id": "scripttask18",
			"name": "manager"
		},
		"c6b99f32-5fe6-4ab6-b60a-80fba1b9ae0f": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow1",
			"name": "SequenceFlow1",
			"sourceRef": "11a9b5ee-17c0-4159-9bbf-454dcfdcd5c3",
			"targetRef": "eb82be40-1748-4f7c-b57f-467349a55bc5"
		},
		"193072bb-4280-4237-90d9-8c0b34baf329": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow2",
			"name": "SequenceFlow2",
			"sourceRef": "63a572e8-83d1-45cc-a506-d1f60ff6efa3",
			"targetRef": "8cb0f833-c772-4bc3-a97d-e9d6b304a187"
		},
		"ab130c07-c299-41c3-b3d0-179b8bac0686": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow8",
			"name": "SequenceFlow8",
			"sourceRef": "b9923f0c-4dcb-4a94-b952-c6dd79ac0b53",
			"targetRef": "eb3462f7-b1a3-476c-8a5a-298c181d394f"
		},
		"18da8bda-9464-480d-a55f-56f364270edf": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow9",
			"name": "SequenceFlow9",
			"sourceRef": "857938bb-3134-42bc-b15b-5b4cd81f0468",
			"targetRef": "9ef63fa4-3e32-4583-8c41-dd96daa68288"
		},
		"e4397020-c1fc-4ffd-bbe2-438e121eb34c": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow10",
			"name": "SequenceFlow10",
			"sourceRef": "bb6fc91e-e94a-430a-b629-d943b174113c",
			"targetRef": "806ecc4e-e1ab-4b8c-99ca-e4320a4947db"
		},
		"78b46b05-66e7-44cc-b390-9830007178d5": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"condition": "${context.sub_workflow_managers_flag == true}",
			"id": "sequenceflow11",
			"name": "check done",
			"sourceRef": "806ecc4e-e1ab-4b8c-99ca-e4320a4947db",
			"targetRef": "09d7bcd0-979a-4c0d-bec3-a530ca6b294c"
		},
		"14831d15-9b06-41bc-b619-c3a2fd7eeda7": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"condition": "${context.approved == true}",
			"id": "sequenceflow19",
			"name": "approve",
			"sourceRef": "3f77562b-a43d-45e7-93f8-21a0c96a2970",
			"targetRef": "bb6fc91e-e94a-430a-b629-d943b174113c"
		},
		"0ed30650-baac-47ce-b3f6-bfca924a7ac5": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow20",
			"name": "rerject",
			"sourceRef": "3f77562b-a43d-45e7-93f8-21a0c96a2970",
			"targetRef": "46338427-c891-4838-9d01-1a8f3faaaa2d"
		},
		"c2d49164-ac3d-44f2-b36d-b1c4d5d92d2f": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow21",
			"name": "SequenceFlow21",
			"sourceRef": "eb3462f7-b1a3-476c-8a5a-298c181d394f",
			"targetRef": "c6875fe3-1a38-41d5-91e2-52f6baf306c7"
		},
		"241ee828-3a05-41e5-9106-78fa0dd2d03c": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow23",
			"name": "SequenceFlow23",
			"sourceRef": "c6875fe3-1a38-41d5-91e2-52f6baf306c7",
			"targetRef": "c7b40236-4dd1-4fea-a49b-8590e5a00868"
		},
		"438d8028-0cc4-4e18-a366-c6ab77df2066": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow24",
			"name": "SequenceFlow24",
			"sourceRef": "c7b40236-4dd1-4fea-a49b-8590e5a00868",
			"targetRef": "b5a144ed-e454-48d1-ac29-d56b0d002d23"
		},
		"9e4c563c-7cca-4345-b6ab-083a0e213d04": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow26",
			"name": "redo",
			"sourceRef": "e2add96e-9362-4104-b558-85ac987bfd81",
			"targetRef": "f7a38666-5271-4cba-ba27-8429ecea87b5"
		},
		"32baee57-85fb-46f1-8415-9bca676b9982": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow27",
			"name": "SequenceFlow27",
			"sourceRef": "cec11a65-a601-48e3-a035-de8f6736f439",
			"targetRef": "e2add96e-9362-4104-b558-85ac987bfd81"
		},
		"b5b79097-1915-43a5-b7ae-7beb17ec7e23": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow28",
			"name": "SequenceFlow28",
			"sourceRef": "b5a144ed-e454-48d1-ac29-d56b0d002d23",
			"targetRef": "cec11a65-a601-48e3-a035-de8f6736f439"
		},
		"710cb275-d76d-4f88-a6ee-f4570489fd4f": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow29",
			"name": "SequenceFlow29",
			"sourceRef": "8cb0f833-c772-4bc3-a97d-e9d6b304a187",
			"targetRef": "3f77562b-a43d-45e7-93f8-21a0c96a2970"
		},
		"5c9c5188-07bf-49af-906a-f0316ce80722": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"condition": "${context.erp_flag == true}",
			"id": "sequenceflow31",
			"name": "End",
			"sourceRef": "e2add96e-9362-4104-b558-85ac987bfd81",
			"targetRef": "cefd449f-53b5-4986-a964-d87f70d94e0b"
		},
		"f592263f-10ab-4d15-982b-c1d53fdb3a69": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow32",
			"name": "SequenceFlow32",
			"sourceRef": "9ef63fa4-3e32-4583-8c41-dd96daa68288",
			"targetRef": "63a572e8-83d1-45cc-a506-d1f60ff6efa3"
		},
		"9490a990-f17f-4793-8b7f-7bb34dae22a7": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow34",
			"name": "SequenceFlow34",
			"sourceRef": "f7a38666-5271-4cba-ba27-8429ecea87b5",
			"targetRef": "b9923f0c-4dcb-4a94-b952-c6dd79ac0b53"
		},
		"f654fdda-45dc-4b70-9e0e-3ce928c2bf48": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow35",
			"name": "SequenceFlow35",
			"sourceRef": "6ee9e69f-3047-482d-b04d-287de6831777",
			"targetRef": "40f112bc-c398-4e72-aacd-26b1e4ee0e95"
		},
		"82250b9e-05a2-44d5-bf5d-b56a3612e8aa": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow36",
			"name": "SequenceFlow36",
			"sourceRef": "cefd449f-53b5-4986-a964-d87f70d94e0b",
			"targetRef": "6ee9e69f-3047-482d-b04d-287de6831777"
		},
		"918aa604-d354-4a7f-993b-d43900c286b7": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow37",
			"name": "SequenceFlow37",
			"sourceRef": "40f112bc-c398-4e72-aacd-26b1e4ee0e95",
			"targetRef": "1d9d4ffa-f23a-4ede-b487-2a3c442dfe19"
		},
		"87dd3c1b-43d9-44c8-bee1-a3632449e679": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow38",
			"name": "SequenceFlow38",
			"sourceRef": "1d9d4ffa-f23a-4ede-b487-2a3c442dfe19",
			"targetRef": "56b5c609-08d6-49d8-9ef7-79545cabdd7d"
		},
		"59de7444-50af-420e-ba74-51cca1918ecd": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"condition": "${context.company_length_i== 0}",
			"id": "sequenceflow39",
			"name": "finished",
			"sourceRef": "56b5c609-08d6-49d8-9ef7-79545cabdd7d",
			"targetRef": "e98ad4bb-5659-41eb-b92a-79e37b99e9a5"
		},
		"321c40f8-33bd-4f08-b66e-0815d38c050e": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow40",
			"name": "redo",
			"sourceRef": "56b5c609-08d6-49d8-9ef7-79545cabdd7d",
			"targetRef": "6ee9e69f-3047-482d-b04d-287de6831777"
		},
		"60ac5b54-1b1c-4dc2-9f4c-f61694671124": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow41",
			"name": "SequenceFlow41",
			"sourceRef": "e98ad4bb-5659-41eb-b92a-79e37b99e9a5",
			"targetRef": "04b8aae2-090f-46ee-b237-bc78f9d74b7d"
		},
		"3bfdb23f-7386-479a-8492-6b00c6598954": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow42",
			"name": "SequenceFlow42",
			"sourceRef": "04b8aae2-090f-46ee-b237-bc78f9d74b7d",
			"targetRef": "d18f4161-4e05-4aeb-becc-a9b2066b62b7"
		},
		"36731c28-b5ce-4868-8bf8-c23a1010aa86": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow43",
			"name": "SequenceFlow43",
			"sourceRef": "d18f4161-4e05-4aeb-becc-a9b2066b62b7",
			"targetRef": "abe59c16-7e58-475a-844c-88cf32261387"
		},
		"256abefa-57e6-4939-abdf-b0ba7fae0ba5": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"condition": "${context.purchase_length_i== 0}",
			"id": "sequenceflow44",
			"name": "finished",
			"sourceRef": "abe59c16-7e58-475a-844c-88cf32261387",
			"targetRef": "2798f4e7-bc42-4fad-a248-159095a2f40a"
		},
		"e80489d6-50f5-4ed2-80d8-2b8ebe6cbf8b": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow45",
			"name": "redo",
			"sourceRef": "abe59c16-7e58-475a-844c-88cf32261387",
			"targetRef": "e98ad4bb-5659-41eb-b92a-79e37b99e9a5"
		},
		"c2aaa7e9-ab45-4e3f-819b-a07948caa8bc": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow46",
			"name": "redi",
			"sourceRef": "806ecc4e-e1ab-4b8c-99ca-e4320a4947db",
			"targetRef": "857938bb-3134-42bc-b15b-5b4cd81f0468"
		},
		"bd440fce-7441-4560-afdd-cf42a834835f": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow47",
			"name": "SequenceFlow47",
			"sourceRef": "09d7bcd0-979a-4c0d-bec3-a530ca6b294c",
			"targetRef": "f7a38666-5271-4cba-ba27-8429ecea87b5"
		},
		"4563a7d3-681b-460a-961f-876d43e18a96": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"condition": "${context.manager_flag == true}",
			"id": "sequenceflow48",
			"name": "SequenceFlow48",
			"sourceRef": "541f65d0-82b3-4fb1-8e6a-5240f6d3708e",
			"targetRef": "857938bb-3134-42bc-b15b-5b4cd81f0468"
		},
		"222ebf59-b472-49b8-9fc9-268441e94571": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow49",
			"name": "SequenceFlow49",
			"sourceRef": "541f65d0-82b3-4fb1-8e6a-5240f6d3708e",
			"targetRef": "09d7bcd0-979a-4c0d-bec3-a530ca6b294c"
		},
		"b1edcd1b-54dd-4dfc-86f9-54a6ea975d89": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow50",
			"name": "SequenceFlow50",
			"sourceRef": "eb82be40-1748-4f7c-b57f-467349a55bc5",
			"targetRef": "541f65d0-82b3-4fb1-8e6a-5240f6d3708e"
		},
		"42fa7a2d-c526-4a02-b3ba-49b5168ba644": {
			"classDefinition": "com.sap.bpm.wfs.ui.Diagram",
			"symbols": {
				"df898b52-91e1-4778-baad-2ad9a261d30e": {},
				"53e54950-7757-4161-82c9-afa7e86cff2c": {},
				"6bb141da-d485-4317-93b8-e17711df4c32": {},
				"5017a4ab-e1cf-49d5-97dd-df696ee0bc1e": {},
				"08cf27e2-5a2b-47a1-8afb-43f3d17b7599": {},
				"2735f197-db37-4ddd-9e61-81204f65ed9c": {},
				"db0fb8ce-557b-4fa7-bf97-4fe5db6017cc": {},
				"b1658f17-1c52-4ec2-9f46-2ffad9c72680": {},
				"9036007b-2eaf-4a01-a410-6661329bcd27": {},
				"6ba3d56e-5917-4767-8232-3d3d86e9712a": {},
				"9bdbc2cd-d92e-420c-9928-1ffb6eac0686": {},
				"46745186-18fb-4aab-8322-8fd998bfdeb8": {},
				"5a966c26-c82b-4d41-8f80-5cbdbaab8f1e": {},
				"448451d6-9ae6-47f9-a1af-985d4a2ac70d": {},
				"32a3a3a2-28c5-4122-86b6-b2a47974c723": {},
				"8be97f9b-06ab-42ff-9073-6838eb7a1e5a": {},
				"fe65def5-8ba2-4b43-abc4-815f66abbaf9": {},
				"8556c695-7628-42e1-9493-13c2a08ca99f": {},
				"d302587f-f559-4fee-8add-156637d0531c": {},
				"fe61ebde-2add-412a-b2ed-2bba3bd16c57": {},
				"aa930e71-53e0-48bb-8583-16723b36d320": {},
				"a4de5631-99da-480a-9925-0ff0d8784776": {},
				"127ff358-3af9-4d49-a27a-49c389b1a75d": {},
				"3ed1cd40-1ea1-49c9-94a0-1cacbae9a9b6": {},
				"c43579d6-e581-4146-985c-218fa7786a7e": {},
				"44915cf7-e4c1-49f7-b807-950a43041aaa": {},
				"6c0bf587-ec1c-4cad-8957-93d88b95ec66": {},
				"ff61fb2c-6600-413f-bb23-0d8231f5e271": {},
				"b73ba8dc-b9ad-4712-92fa-232ccd41092f": {},
				"6b7c76db-8b69-48d2-8d62-3b5d28f43452": {},
				"3f0d68c6-6f75-473f-a9fe-5f8c0d0b17d4": {},
				"01695b8f-ea09-46f0-837a-f827d827baff": {},
				"87c71135-f7eb-48cc-a08a-5c0d4aafe23c": {},
				"71d8235a-0df7-46ef-b64a-686bf0607f90": {},
				"f0c91d04-f6c5-4944-a23b-9cec46f4cb1e": {},
				"ac06938e-77db-4cd9-92cc-7a4a0fea8701": {},
				"70238f0b-1c1a-45f6-aa8f-443fd39c81be": {},
				"4c9a0e5c-0f02-4e68-81f2-67b0abdb869a": {},
				"f091de6a-bcb7-4cc8-a99d-770f9dde8540": {},
				"54e9b84d-fd08-4adb-b575-89979dcdce9f": {},
				"88740ea4-006d-4114-beef-1adeb47226f4": {},
				"76ade43b-1a08-41ff-b7ab-c86ccda6c0e7": {},
				"3bec94bc-b86c-4b7d-8bec-1a968146fcb8": {},
				"8aa9b23e-6eb3-4833-9f3e-f6337bd3a592": {},
				"da614aa4-54b3-49aa-986f-8266e9f645ba": {},
				"2949bd1d-d216-4558-abd3-446148334969": {},
				"28b7bb31-990d-4a98-b998-f930a0df8bd0": {},
				"3056da32-c9b5-42f0-b3f0-f7e740e12b97": {},
				"46ae803d-d835-45e2-9a96-278f27a4ac53": {},
				"d1f35951-9571-445c-bf2a-d24e8aefc314": {},
				"f3f2baef-bd98-4534-9d4b-678c9429d427": {},
				"e5e4f241-9e68-452c-8ee3-c63ad65d6777": {},
				"cf93b203-47d3-4b75-b90b-bb438ff8fd34": {},
				"dc821a3d-f7d1-43a2-ab8e-304d7c415a58": {},
				"c5ea23fa-1b2b-465c-acee-f12053a72efb": {},
				"b4df120e-792e-4a91-bfdf-f284ed8c9e53": {},
				"9dd7232f-7f7b-44d8-b6dd-1d18f45ec2df": {},
				"b9a3df88-1c07-4c7c-b8c6-41e459b60ac7": {},
				"99a4f7c8-7d48-4050-b721-eaf68ef92089": {},
				"7851c9fc-616c-4994-8a6b-f3449556dafc": {},
				"6cb66272-c7ce-47f3-a3e4-52a9bc02edae": {},
				"38f2f7ca-d17b-4324-860f-d4031400f3f0": {},
				"53961bb4-1e2c-4ec9-ac7d-83f87902af2a": {},
				"ecd3e66a-497c-44b5-b0f8-7ae2c7839044": {}
			}
		},
		"b627eeab-ad22-498a-ada8-79d683ee4a7e": {
			"classDefinition": "com.sap.bpm.wfs.TerminateEventDefinition",
			"id": "terminateeventdefinition1"
		},
		"54eb74e6-291b-4f89-a6b7-f56fc9f93c43": {
			"classDefinition": "com.sap.bpm.wfs.MessageEventDefinition",
			"responseVariable": "${context.erp_message}",
			"id": "messageeventdefinition2",
			"messageRef": "285a6446-3fd6-44ef-b893-7b40c4b9696d"
		},
		"df898b52-91e1-4778-baad-2ad9a261d30e": {
			"classDefinition": "com.sap.bpm.wfs.ui.StartEventSymbol",
			"x": -464,
			"y": 27,
			"width": 32,
			"height": 32,
			"object": "11a9b5ee-17c0-4159-9bbf-454dcfdcd5c3"
		},
		"53e54950-7757-4161-82c9-afa7e86cff2c": {
			"classDefinition": "com.sap.bpm.wfs.ui.EndEventSymbol",
			"x": 2801,
			"y": 24.5,
			"width": 35,
			"height": 35,
			"object": "2798f4e7-bc42-4fad-a248-159095a2f40a"
		},
		"6bb141da-d485-4317-93b8-e17711df4c32": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "-448,42.5 -357,42.5",
			"sourceSymbol": "df898b52-91e1-4778-baad-2ad9a261d30e",
			"targetSymbol": "53961bb4-1e2c-4ec9-ac7d-83f87902af2a",
			"object": "c6b99f32-5fe6-4ab6-b60a-80fba1b9ae0f"
		},
		"5017a4ab-e1cf-49d5-97dd-df696ee0bc1e": {
			"classDefinition": "com.sap.bpm.wfs.ui.UserTaskSymbol",
			"x": 137,
			"y": 12,
			"width": 100,
			"height": 60,
			"object": "63a572e8-83d1-45cc-a506-d1f60ff6efa3"
		},
		"08cf27e2-5a2b-47a1-8afb-43f3d17b7599": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "187,42 322,42",
			"sourceSymbol": "5017a4ab-e1cf-49d5-97dd-df696ee0bc1e",
			"targetSymbol": "6b7c76db-8b69-48d2-8d62-3b5d28f43452",
			"object": "193072bb-4280-4237-90d9-8c0b34baf329"
		},
		"2735f197-db37-4ddd-9e61-81204f65ed9c": {
			"classDefinition": "com.sap.bpm.wfs.ui.ScriptTaskSymbol",
			"x": -163,
			"y": 12,
			"width": 100,
			"height": 60,
			"object": "857938bb-3134-42bc-b15b-5b4cd81f0468"
		},
		"db0fb8ce-557b-4fa7-bf97-4fe5db6017cc": {
			"classDefinition": "com.sap.bpm.wfs.ui.ScriptTaskSymbol",
			"x": 480,
			"y": 12,
			"width": 100,
			"height": 60,
			"object": "bb6fc91e-e94a-430a-b629-d943b174113c"
		},
		"b1658f17-1c52-4ec2-9f46-2ffad9c72680": {
			"classDefinition": "com.sap.bpm.wfs.ui.UserTaskSymbol",
			"x": 856,
			"y": 12,
			"width": 100,
			"height": 60,
			"object": "b9923f0c-4dcb-4a94-b952-c6dd79ac0b53"
		},
		"9036007b-2eaf-4a01-a410-6661329bcd27": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "906,42 1024,42",
			"sourceSymbol": "b1658f17-1c52-4ec2-9f46-2ffad9c72680",
			"targetSymbol": "8556c695-7628-42e1-9493-13c2a08ca99f",
			"object": "ab130c07-c299-41c3-b3d0-179b8bac0686"
		},
		"6ba3d56e-5917-4767-8232-3d3d86e9712a": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "-113,42 32,42",
			"sourceSymbol": "2735f197-db37-4ddd-9e61-81204f65ed9c",
			"targetSymbol": "87c71135-f7eb-48cc-a08a-5c0d4aafe23c",
			"object": "18da8bda-9464-480d-a55f-56f364270edf"
		},
		"9bdbc2cd-d92e-420c-9928-1ffb6eac0686": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "530,44 632,44",
			"sourceSymbol": "db0fb8ce-557b-4fa7-bf97-4fe5db6017cc",
			"targetSymbol": "46745186-18fb-4aab-8322-8fd998bfdeb8",
			"object": "e4397020-c1fc-4ffd-bbe2-438e121eb34c"
		},
		"46745186-18fb-4aab-8322-8fd998bfdeb8": {
			"classDefinition": "com.sap.bpm.wfs.ui.ExclusiveGatewaySymbol",
			"x": 611,
			"y": 25,
			"object": "806ecc4e-e1ab-4b8c-99ca-e4320a4947db"
		},
		"5a966c26-c82b-4d41-8f80-5cbdbaab8f1e": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "632,44 689,44",
			"sourceSymbol": "46745186-18fb-4aab-8322-8fd998bfdeb8",
			"targetSymbol": "b9a3df88-1c07-4c7c-b8c6-41e459b60ac7",
			"object": "78b46b05-66e7-44cc-b390-9830007178d5"
		},
		"448451d6-9ae6-47f9-a1af-985d4a2ac70d": {
			"classDefinition": "com.sap.bpm.wfs.ui.ExclusiveGatewaySymbol",
			"x": 410,
			"y": 25,
			"object": "3f77562b-a43d-45e7-93f8-21a0c96a2970"
		},
		"32a3a3a2-28c5-4122-86b6-b2a47974c723": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "431,44 530,44",
			"sourceSymbol": "448451d6-9ae6-47f9-a1af-985d4a2ac70d",
			"targetSymbol": "db0fb8ce-557b-4fa7-bf97-4fe5db6017cc",
			"object": "14831d15-9b06-41bc-b619-c3a2fd7eeda7"
		},
		"8be97f9b-06ab-42ff-9073-6838eb7a1e5a": {
			"classDefinition": "com.sap.bpm.wfs.ui.EndEventSymbol",
			"x": 413.5,
			"y": 139.5,
			"width": 35,
			"height": 35,
			"object": "46338427-c891-4838-9d01-1a8f3faaaa2d"
		},
		"fe65def5-8ba2-4b43-abc4-815f66abbaf9": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "431,46 431,148",
			"sourceSymbol": "448451d6-9ae6-47f9-a1af-985d4a2ac70d",
			"targetSymbol": "8be97f9b-06ab-42ff-9073-6838eb7a1e5a",
			"object": "0ed30650-baac-47ce-b3f6-bfca924a7ac5"
		},
		"8556c695-7628-42e1-9493-13c2a08ca99f": {
			"classDefinition": "com.sap.bpm.wfs.ui.ScriptTaskSymbol",
			"x": 974,
			"y": 12,
			"width": 100,
			"height": 60,
			"object": "eb3462f7-b1a3-476c-8a5a-298c181d394f"
		},
		"d302587f-f559-4fee-8add-156637d0531c": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "1024,42 1144,42",
			"sourceSymbol": "8556c695-7628-42e1-9493-13c2a08ca99f",
			"targetSymbol": "fe61ebde-2add-412a-b2ed-2bba3bd16c57",
			"object": "c2d49164-ac3d-44f2-b36d-b1c4d5d92d2f"
		},
		"fe61ebde-2add-412a-b2ed-2bba3bd16c57": {
			"classDefinition": "com.sap.bpm.wfs.ui.ScriptTaskSymbol",
			"x": 1094,
			"y": 12,
			"width": 100,
			"height": 60,
			"object": "c6875fe3-1a38-41d5-91e2-52f6baf306c7"
		},
		"aa930e71-53e0-48bb-8583-16723b36d320": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "1144,42 1265,42",
			"sourceSymbol": "fe61ebde-2add-412a-b2ed-2bba3bd16c57",
			"targetSymbol": "a4de5631-99da-480a-9925-0ff0d8784776",
			"object": "241ee828-3a05-41e5-9106-78fa0dd2d03c"
		},
		"a4de5631-99da-480a-9925-0ff0d8784776": {
			"classDefinition": "com.sap.bpm.wfs.ui.ServiceTaskSymbol",
			"x": 1215,
			"y": 12,
			"width": 100,
			"height": 60,
			"object": "c7b40236-4dd1-4fea-a49b-8590e5a00868"
		},
		"127ff358-3af9-4d49-a27a-49c389b1a75d": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "1265,41 1376,41",
			"sourceSymbol": "a4de5631-99da-480a-9925-0ff0d8784776",
			"targetSymbol": "ff61fb2c-6600-413f-bb23-0d8231f5e271",
			"object": "438d8028-0cc4-4e18-a366-c6ab77df2066"
		},
		"3ed1cd40-1ea1-49c9-94a0-1cacbae9a9b6": {
			"classDefinition": "com.sap.bpm.wfs.ui.ExclusiveGatewaySymbol",
			"x": 1591,
			"y": 21,
			"object": "e2add96e-9362-4104-b558-85ac987bfd81"
		},
		"c43579d6-e581-4146-985c-218fa7786a7e": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "1612,42 1612,-38.5 782,-38.5 782,12.5",
			"sourceSymbol": "3ed1cd40-1ea1-49c9-94a0-1cacbae9a9b6",
			"targetSymbol": "f0c91d04-f6c5-4944-a23b-9cec46f4cb1e",
			"object": "9e4c563c-7cca-4345-b6ab-083a0e213d04"
		},
		"44915cf7-e4c1-49f7-b807-950a43041aaa": {
			"classDefinition": "com.sap.bpm.wfs.ui.ScriptTaskSymbol",
			"x": 1442,
			"y": 12,
			"width": 100,
			"height": 60,
			"object": "cec11a65-a601-48e3-a035-de8f6736f439"
		},
		"6c0bf587-ec1c-4cad-8957-93d88b95ec66": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "1492,42 1612,42",
			"sourceSymbol": "44915cf7-e4c1-49f7-b807-950a43041aaa",
			"targetSymbol": "3ed1cd40-1ea1-49c9-94a0-1cacbae9a9b6",
			"object": "32baee57-85fb-46f1-8415-9bca676b9982"
		},
		"ff61fb2c-6600-413f-bb23-0d8231f5e271": {
			"classDefinition": "com.sap.bpm.wfs.ui.IntermediateCatchEventSymbol",
			"x": 1360,
			"y": 24,
			"width": 32,
			"height": 32,
			"object": "b5a144ed-e454-48d1-ac29-d56b0d002d23"
		},
		"b73ba8dc-b9ad-4712-92fa-232ccd41092f": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "1376,41 1492,41",
			"sourceSymbol": "ff61fb2c-6600-413f-bb23-0d8231f5e271",
			"targetSymbol": "44915cf7-e4c1-49f7-b807-950a43041aaa",
			"object": "b5b79097-1915-43a5-b7ae-7beb17ec7e23"
		},
		"6b7c76db-8b69-48d2-8d62-3b5d28f43452": {
			"classDefinition": "com.sap.bpm.wfs.ui.ScriptTaskSymbol",
			"x": 272,
			"y": 12,
			"width": 100,
			"height": 60,
			"object": "8cb0f833-c772-4bc3-a97d-e9d6b304a187"
		},
		"3f0d68c6-6f75-473f-a9fe-5f8c0d0b17d4": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "322,44 431,44",
			"sourceSymbol": "6b7c76db-8b69-48d2-8d62-3b5d28f43452",
			"targetSymbol": "448451d6-9ae6-47f9-a1af-985d4a2ac70d",
			"object": "710cb275-d76d-4f88-a6ee-f4570489fd4f"
		},
		"01695b8f-ea09-46f0-837a-f827d827baff": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "1612,42 1693,42",
			"sourceSymbol": "3ed1cd40-1ea1-49c9-94a0-1cacbae9a9b6",
			"targetSymbol": "f091de6a-bcb7-4cc8-a99d-770f9dde8540",
			"object": "5c9c5188-07bf-49af-906a-f0316ce80722"
		},
		"87c71135-f7eb-48cc-a08a-5c0d4aafe23c": {
			"classDefinition": "com.sap.bpm.wfs.ui.MailTaskSymbol",
			"x": -18,
			"y": 12,
			"width": 100,
			"height": 60,
			"object": "9ef63fa4-3e32-4583-8c41-dd96daa68288"
		},
		"71d8235a-0df7-46ef-b64a-686bf0607f90": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "32,42 187,42",
			"sourceSymbol": "87c71135-f7eb-48cc-a08a-5c0d4aafe23c",
			"targetSymbol": "5017a4ab-e1cf-49d5-97dd-df696ee0bc1e",
			"object": "f592263f-10ab-4d15-982b-c1d53fdb3a69"
		},
		"f0c91d04-f6c5-4944-a23b-9cec46f4cb1e": {
			"classDefinition": "com.sap.bpm.wfs.ui.MailTaskSymbol",
			"x": 732,
			"y": 12,
			"width": 100,
			"height": 60,
			"object": "f7a38666-5271-4cba-ba27-8429ecea87b5"
		},
		"ac06938e-77db-4cd9-92cc-7a4a0fea8701": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "782,42 906,42",
			"sourceSymbol": "f0c91d04-f6c5-4944-a23b-9cec46f4cb1e",
			"targetSymbol": "b1658f17-1c52-4ec2-9f46-2ffad9c72680",
			"object": "9490a990-f17f-4793-8b7f-7bb34dae22a7"
		},
		"70238f0b-1c1a-45f6-aa8f-443fd39c81be": {
			"classDefinition": "com.sap.bpm.wfs.ui.ScriptTaskSymbol",
			"x": 1763,
			"y": 12,
			"width": 100,
			"height": 60,
			"object": "6ee9e69f-3047-482d-b04d-287de6831777"
		},
		"4c9a0e5c-0f02-4e68-81f2-67b0abdb869a": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "1813,42 1941,42",
			"sourceSymbol": "70238f0b-1c1a-45f6-aa8f-443fd39c81be",
			"targetSymbol": "88740ea4-006d-4114-beef-1adeb47226f4",
			"object": "f654fdda-45dc-4b70-9e0e-3ce928c2bf48"
		},
		"f091de6a-bcb7-4cc8-a99d-770f9dde8540": {
			"classDefinition": "com.sap.bpm.wfs.ui.ScriptTaskSymbol",
			"x": 1643,
			"y": 12,
			"width": 100,
			"height": 60,
			"object": "cefd449f-53b5-4986-a964-d87f70d94e0b"
		},
		"54e9b84d-fd08-4adb-b575-89979dcdce9f": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "1693,42 1813,42",
			"sourceSymbol": "f091de6a-bcb7-4cc8-a99d-770f9dde8540",
			"targetSymbol": "70238f0b-1c1a-45f6-aa8f-443fd39c81be",
			"object": "82250b9e-05a2-44d5-bf5d-b56a3612e8aa"
		},
		"88740ea4-006d-4114-beef-1adeb47226f4": {
			"classDefinition": "com.sap.bpm.wfs.ui.ServiceTaskSymbol",
			"x": 1891,
			"y": 12,
			"width": 100,
			"height": 60,
			"object": "40f112bc-c398-4e72-aacd-26b1e4ee0e95"
		},
		"76ade43b-1a08-41ff-b7ab-c86ccda6c0e7": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "1941,42 2073,42",
			"sourceSymbol": "88740ea4-006d-4114-beef-1adeb47226f4",
			"targetSymbol": "3bec94bc-b86c-4b7d-8bec-1a968146fcb8",
			"object": "918aa604-d354-4a7f-993b-d43900c286b7"
		},
		"3bec94bc-b86c-4b7d-8bec-1a968146fcb8": {
			"classDefinition": "com.sap.bpm.wfs.ui.ScriptTaskSymbol",
			"x": 2023,
			"y": 12,
			"width": 100,
			"height": 60,
			"object": "1d9d4ffa-f23a-4ede-b487-2a3c442dfe19"
		},
		"8aa9b23e-6eb3-4833-9f3e-f6337bd3a592": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "2073,42 2190,42",
			"sourceSymbol": "3bec94bc-b86c-4b7d-8bec-1a968146fcb8",
			"targetSymbol": "da614aa4-54b3-49aa-986f-8266e9f645ba",
			"object": "87dd3c1b-43d9-44c8-bee1-a3632449e679"
		},
		"da614aa4-54b3-49aa-986f-8266e9f645ba": {
			"classDefinition": "com.sap.bpm.wfs.ui.ExclusiveGatewaySymbol",
			"x": 2169,
			"y": 21,
			"object": "56b5c609-08d6-49d8-9ef7-79545cabdd7d"
		},
		"2949bd1d-d216-4558-abd3-446148334969": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "2190,41 2317,41",
			"sourceSymbol": "da614aa4-54b3-49aa-986f-8266e9f645ba",
			"targetSymbol": "3056da32-c9b5-42f0-b3f0-f7e740e12b97",
			"object": "59de7444-50af-420e-ba74-51cca1918ecd"
		},
		"28b7bb31-990d-4a98-b998-f930a0df8bd0": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "2190,42 2190,-37.75 1813,-37.75 1813,12.5",
			"sourceSymbol": "da614aa4-54b3-49aa-986f-8266e9f645ba",
			"targetSymbol": "70238f0b-1c1a-45f6-aa8f-443fd39c81be",
			"object": "321c40f8-33bd-4f08-b66e-0815d38c050e"
		},
		"3056da32-c9b5-42f0-b3f0-f7e740e12b97": {
			"classDefinition": "com.sap.bpm.wfs.ui.ScriptTaskSymbol",
			"x": 2267,
			"y": 12,
			"width": 100,
			"height": 60,
			"object": "e98ad4bb-5659-41eb-b92a-79e37b99e9a5"
		},
		"46ae803d-d835-45e2-9a96-278f27a4ac53": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "2317,42 2453,42",
			"sourceSymbol": "3056da32-c9b5-42f0-b3f0-f7e740e12b97",
			"targetSymbol": "d1f35951-9571-445c-bf2a-d24e8aefc314",
			"object": "60ac5b54-1b1c-4dc2-9f4c-f61694671124"
		},
		"d1f35951-9571-445c-bf2a-d24e8aefc314": {
			"classDefinition": "com.sap.bpm.wfs.ui.ServiceTaskSymbol",
			"x": 2403,
			"y": 12,
			"width": 100,
			"height": 60,
			"object": "04b8aae2-090f-46ee-b237-bc78f9d74b7d"
		},
		"f3f2baef-bd98-4534-9d4b-678c9429d427": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "2453,42 2584,42",
			"sourceSymbol": "d1f35951-9571-445c-bf2a-d24e8aefc314",
			"targetSymbol": "e5e4f241-9e68-452c-8ee3-c63ad65d6777",
			"object": "3bfdb23f-7386-479a-8492-6b00c6598954"
		},
		"e5e4f241-9e68-452c-8ee3-c63ad65d6777": {
			"classDefinition": "com.sap.bpm.wfs.ui.ScriptTaskSymbol",
			"x": 2534,
			"y": 12,
			"width": 100,
			"height": 60,
			"object": "d18f4161-4e05-4aeb-becc-a9b2066b62b7"
		},
		"cf93b203-47d3-4b75-b90b-bb438ff8fd34": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "2584,42 2699,42",
			"sourceSymbol": "e5e4f241-9e68-452c-8ee3-c63ad65d6777",
			"targetSymbol": "dc821a3d-f7d1-43a2-ab8e-304d7c415a58",
			"object": "36731c28-b5ce-4868-8bf8-c23a1010aa86"
		},
		"dc821a3d-f7d1-43a2-ab8e-304d7c415a58": {
			"classDefinition": "com.sap.bpm.wfs.ui.ExclusiveGatewaySymbol",
			"x": 2678,
			"y": 21,
			"object": "abe59c16-7e58-475a-844c-88cf32261387"
		},
		"c5ea23fa-1b2b-465c-acee-f12053a72efb": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "2699,42 2818.5,42",
			"sourceSymbol": "dc821a3d-f7d1-43a2-ab8e-304d7c415a58",
			"targetSymbol": "53e54950-7757-4161-82c9-afa7e86cff2c",
			"object": "256abefa-57e6-4939-abdf-b0ba7fae0ba5"
		},
		"b4df120e-792e-4a91-bfdf-f284ed8c9e53": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "2699,42 2699,-36.75 2317,-36.75 2317,12.5",
			"sourceSymbol": "dc821a3d-f7d1-43a2-ab8e-304d7c415a58",
			"targetSymbol": "3056da32-c9b5-42f0-b3f0-f7e740e12b97",
			"object": "e80489d6-50f5-4ed2-80d8-2b8ebe6cbf8b"
		},
		"9dd7232f-7f7b-44d8-b6dd-1d18f45ec2df": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "632,46 632,-37.75 -113,-37.75 -113,12.5",
			"sourceSymbol": "46745186-18fb-4aab-8322-8fd998bfdeb8",
			"targetSymbol": "2735f197-db37-4ddd-9e61-81204f65ed9c",
			"object": "c2aaa7e9-ab45-4e3f-819b-a07948caa8bc"
		},
		"b9a3df88-1c07-4c7c-b8c6-41e459b60ac7": {
			"classDefinition": "com.sap.bpm.wfs.ui.ScriptTaskSymbol",
			"x": 664,
			"y": 12,
			"width": 50,
			"height": 60,
			"object": "09d7bcd0-979a-4c0d-bec3-a530ca6b294c"
		},
		"99a4f7c8-7d48-4050-b721-eaf68ef92089": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "689,42 782,42",
			"sourceSymbol": "b9a3df88-1c07-4c7c-b8c6-41e459b60ac7",
			"targetSymbol": "f0c91d04-f6c5-4944-a23b-9cec46f4cb1e",
			"object": "bd440fce-7441-4560-afdd-cf42a834835f"
		},
		"7851c9fc-616c-4994-8a6b-f3449556dafc": {
			"classDefinition": "com.sap.bpm.wfs.ui.ExclusiveGatewaySymbol",
			"x": -279,
			"y": 22,
			"object": "541f65d0-82b3-4fb1-8e6a-5240f6d3708e"
		},
		"6cb66272-c7ce-47f3-a3e4-52a9bc02edae": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "-258,42.5 -113,42.5",
			"sourceSymbol": "7851c9fc-616c-4994-8a6b-f3449556dafc",
			"targetSymbol": "2735f197-db37-4ddd-9e61-81204f65ed9c",
			"object": "4563a7d3-681b-460a-961f-876d43e18a96"
		},
		"38f2f7ca-d17b-4324-860f-d4031400f3f0": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "-258,63.5 -258,204 693,204 693,69",
			"sourceSymbol": "7851c9fc-616c-4994-8a6b-f3449556dafc",
			"targetSymbol": "b9a3df88-1c07-4c7c-b8c6-41e459b60ac7",
			"object": "222ebf59-b472-49b8-9fc9-268441e94571"
		},
		"53961bb4-1e2c-4ec9-ac7d-83f87902af2a": {
			"classDefinition": "com.sap.bpm.wfs.ui.ScriptTaskSymbol",
			"x": -407,
			"y": 12,
			"width": 100,
			"height": 60,
			"object": "eb82be40-1748-4f7c-b57f-467349a55bc5"
		},
		"ecd3e66a-497c-44b5-b0f8-7ae2c7839044": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "-357,42.5 -258,42.5",
			"sourceSymbol": "53961bb4-1e2c-4ec9-ac7d-83f87902af2a",
			"targetSymbol": "7851c9fc-616c-4994-8a6b-f3449556dafc",
			"object": "b1edcd1b-54dd-4dfc-86f9-54a6ea975d89"
		},
		"62d7f4ed-4063-4c44-af8b-39050bd44926": {
			"classDefinition": "com.sap.bpm.wfs.LastIDs",
			"terminateeventdefinition": 1,
			"messageeventdefinition": 2,
			"message": 2,
			"maildefinition": 3,
			"sequenceflow": 50,
			"startevent": 1,
			"intermediatemessageevent": 2,
			"endevent": 2,
			"usertask": 3,
			"servicetask": 5,
			"scripttask": 18,
			"mailtask": 2,
			"exclusivegateway": 7
		},
		"285a6446-3fd6-44ef-b893-7b40c4b9696d": {
			"classDefinition": "com.sap.bpm.wfs.Message",
			"name": "compur",
			"id": "message2"
		},
		"34a584da-b829-4a46-b16e-8eb5a3d9ed7f": {
			"classDefinition": "com.sap.bpm.wfs.MailDefinition",
			"name": "maildefinition2",
			"to": "${context.site_fin_mail}",
			"subject": "There’s a new vendor application form that needs your approval.",
			"text": "Hi \nA Vendor Code Application(Doc: No: ${context.no} )\nApplicant: ${context.applicant} requires  needs your approval\nVendor Name: ${context.vendorid}\n\nPlease click the following hyper link to open document for approval.\n\nhttps://wistronprework.cockpit.workflowmanagement.cfapps.eu10.hana.ondemand.com/cp.portal/site#WorkflowTask-DisplayMyInbox?sap-ui-app-id-hint=cross.fnd.fiori.inbox&substitution=true&userSearch=false&/detail/NA/ec412890-0b47-11ed-806d-eeee0a95fab7/TaskCollection(SAP__Origin='NA',InstanceID='ec412890-0b47-11ed-806d-eeee0a95fab7')\n\nNote:Once you have processed this document, the link above will become inactive!\n\nVCF(Vendor Code Form System)",
			"id": "maildefinition2"
		},
		"f59ceb2b-b093-4781-b84b-87b93b259b2d": {
			"classDefinition": "com.sap.bpm.wfs.MailDefinition",
			"name": "maildefinition3",
			"to": "${context.managers_mail}",
			"subject": "There’s a new vendor application form that needs your approval.",
			"text": "Hi \nA Vendor Code Application(Doc: No: ${context.no} )\nApplicant: ${context.applicant} requires  needs your approval\nVendor Name: ${context.vendorid}\n\nPlease click the following hyper link to open document for approval.\n\nhttps://wistronprework.cockpit.workflowmanagement.cfapps.eu10.hana.ondemand.com/cp.portal/site#WorkflowTask-DisplayMyInbox?sap-ui-app-id-hint=cross.fnd.fiori.inbox&substitution=true&userSearch=false&/detail/NA/ec412890-0b47-11ed-806d-eeee0a95fab7/TaskCollection(SAP__Origin='NA',InstanceID='ec412890-0b47-11ed-806d-eeee0a95fab7')\n\nNote:Once you have processed this document, the link above will become inactive!\n\nVCF(Vendor Code Form System)",
			"id": "maildefinition3"
		}
	}
}