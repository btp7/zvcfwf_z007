{
	"contents": {
		"2b9347b1-078b-471a-a2ff-e6ad7ff0dfbd": {
			"classDefinition": "com.sap.bpm.wfs.Model",
			"id": "wf_z007_usertask",
			"subject": "wf_z007_usertask",
			"name": "wf_z007_usertask",
			"documentation": "wf_z007_usertask",
			"lastIds": "62d7f4ed-4063-4c44-af8b-39050bd44926",
			"events": {
				"11a9b5ee-17c0-4159-9bbf-454dcfdcd5c3": {
					"name": "StartEvent1"
				},
				"2798f4e7-bc42-4fad-a248-159095a2f40a": {
					"name": "EndEvent1"
				}
			},
			"activities": {
				"69a1159f-cc67-47cf-b5cd-9d5a3d87e032": {
					"name": "Application for Vendor Code Form(MRO)"
				},
				"a82dcc03-afda-4346-a268-b87da85c802d": {
					"name": "MRO Mail"
				},
				"e4bcba01-94c4-49dc-9e17-208cf817df80": {
					"name": "MRO Reason"
				},
				"82d26ecf-a7ec-46db-a921-7a9ae66fcfac": {
					"name": "data"
				}
			},
			"sequenceFlows": {
				"c6b99f32-5fe6-4ab6-b60a-80fba1b9ae0f": {
					"name": "SequenceFlow1"
				},
				"5d2a5ca8-e1e2-498e-b8c9-88f93a312865": {
					"name": "SequenceFlow2"
				},
				"fa606f74-d6f5-46b3-bbfc-21f90c4e9c3e": {
					"name": "SequenceFlow3"
				},
				"19beab99-2ad0-488a-a3dd-a3bb8a005ede": {
					"name": "SequenceFlow4"
				},
				"aa4b72d6-7380-4c52-9e4d-6befee9817ee": {
					"name": "SequenceFlow7"
				}
			},
			"diagrams": {
				"42fa7a2d-c526-4a02-b3ba-49b5168ba644": {}
			}
		},
		"11a9b5ee-17c0-4159-9bbf-454dcfdcd5c3": {
			"classDefinition": "com.sap.bpm.wfs.StartEvent",
			"id": "startevent1",
			"name": "StartEvent1"
		},
		"2798f4e7-bc42-4fad-a248-159095a2f40a": {
			"classDefinition": "com.sap.bpm.wfs.EndEvent",
			"id": "endevent1",
			"name": "EndEvent1"
		},
		"69a1159f-cc67-47cf-b5cd-9d5a3d87e032": {
			"classDefinition": "com.sap.bpm.wfs.UserTask",
			"subject": "MRO",
			"priority": "MEDIUM",
			"isHiddenInLogForParticipant": false,
			"supportsForward": true,
			"userInterface": "sapui5://zvcfwf_z007_approuter.zvcfwfz007workflowuimodule/zvcfwfz007workflowuimodule",
			"recipientUsers": "${context.mro_email}",
			"id": "usertask1",
			"name": "Application for Vendor Code Form(MRO)"
		},
		"a82dcc03-afda-4346-a268-b87da85c802d": {
			"classDefinition": "com.sap.bpm.wfs.MailTask",
			"destinationSource": "consumer",
			"id": "mailtask1",
			"name": "MRO Mail",
			"mailDefinitionRef": "951afaee-7329-4638-86a9-18a72f265a1b"
		},
		"e4bcba01-94c4-49dc-9e17-208cf817df80": {
			"classDefinition": "com.sap.bpm.wfs.ScriptTask",
			"reference": "/scripts/wf_z007/script_reason.js",
			"id": "scripttask1",
			"name": "MRO Reason"
		},
		"c6b99f32-5fe6-4ab6-b60a-80fba1b9ae0f": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow1",
			"name": "SequenceFlow1",
			"sourceRef": "11a9b5ee-17c0-4159-9bbf-454dcfdcd5c3",
			"targetRef": "a82dcc03-afda-4346-a268-b87da85c802d"
		},
		"5d2a5ca8-e1e2-498e-b8c9-88f93a312865": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow2",
			"name": "SequenceFlow2",
			"sourceRef": "69a1159f-cc67-47cf-b5cd-9d5a3d87e032",
			"targetRef": "e4bcba01-94c4-49dc-9e17-208cf817df80"
		},
		"fa606f74-d6f5-46b3-bbfc-21f90c4e9c3e": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow3",
			"name": "SequenceFlow3",
			"sourceRef": "a82dcc03-afda-4346-a268-b87da85c802d",
			"targetRef": "69a1159f-cc67-47cf-b5cd-9d5a3d87e032"
		},
		"19beab99-2ad0-488a-a3dd-a3bb8a005ede": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow4",
			"name": "SequenceFlow4",
			"sourceRef": "e4bcba01-94c4-49dc-9e17-208cf817df80",
			"targetRef": "82d26ecf-a7ec-46db-a921-7a9ae66fcfac"
		},
		"42fa7a2d-c526-4a02-b3ba-49b5168ba644": {
			"classDefinition": "com.sap.bpm.wfs.ui.Diagram",
			"symbols": {
				"df898b52-91e1-4778-baad-2ad9a261d30e": {},
				"53e54950-7757-4161-82c9-afa7e86cff2c": {},
				"6bb141da-d485-4317-93b8-e17711df4c32": {},
				"6c7b328f-b1ed-4761-a51a-194f326f5aaf": {},
				"5716c55a-7f9f-4a80-894d-2a210bd6257b": {},
				"e72c6348-3b90-4c4a-a8c7-3dd9af79be89": {},
				"0aeb6fd3-4688-4bbc-8b8f-035eddbceab5": {},
				"0bc3e414-59da-4af3-abd0-3740c8d22816": {},
				"273c21c9-0040-4765-aebb-614ae7471927": {},
				"c1d4de29-497d-4fcd-b645-1dc61b426044": {},
				"a6cc108f-f72d-4559-8595-1658b646ae12": {}
			}
		},
		"df898b52-91e1-4778-baad-2ad9a261d30e": {
			"classDefinition": "com.sap.bpm.wfs.ui.StartEventSymbol",
			"x": -72,
			"y": 100,
			"width": 32,
			"height": 32,
			"object": "11a9b5ee-17c0-4159-9bbf-454dcfdcd5c3"
		},
		"53e54950-7757-4161-82c9-afa7e86cff2c": {
			"classDefinition": "com.sap.bpm.wfs.ui.EndEventSymbol",
			"x": 619,
			"y": 98,
			"width": 35,
			"height": 35,
			"object": "2798f4e7-bc42-4fad-a248-159095a2f40a"
		},
		"6bb141da-d485-4317-93b8-e17711df4c32": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "-56,116 68,116",
			"sourceSymbol": "df898b52-91e1-4778-baad-2ad9a261d30e",
			"targetSymbol": "e72c6348-3b90-4c4a-a8c7-3dd9af79be89",
			"object": "c6b99f32-5fe6-4ab6-b60a-80fba1b9ae0f"
		},
		"6c7b328f-b1ed-4761-a51a-194f326f5aaf": {
			"classDefinition": "com.sap.bpm.wfs.ui.UserTaskSymbol",
			"x": 180,
			"y": 86,
			"width": 100,
			"height": 60,
			"object": "69a1159f-cc67-47cf-b5cd-9d5a3d87e032"
		},
		"5716c55a-7f9f-4a80-894d-2a210bd6257b": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "230,116 385,116",
			"sourceSymbol": "6c7b328f-b1ed-4761-a51a-194f326f5aaf",
			"targetSymbol": "0bc3e414-59da-4af3-abd0-3740c8d22816",
			"object": "5d2a5ca8-e1e2-498e-b8c9-88f93a312865"
		},
		"e72c6348-3b90-4c4a-a8c7-3dd9af79be89": {
			"classDefinition": "com.sap.bpm.wfs.ui.MailTaskSymbol",
			"x": 18,
			"y": 86,
			"width": 100,
			"height": 60,
			"object": "a82dcc03-afda-4346-a268-b87da85c802d"
		},
		"0aeb6fd3-4688-4bbc-8b8f-035eddbceab5": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "68,116 230,116",
			"sourceSymbol": "e72c6348-3b90-4c4a-a8c7-3dd9af79be89",
			"targetSymbol": "6c7b328f-b1ed-4761-a51a-194f326f5aaf",
			"object": "fa606f74-d6f5-46b3-bbfc-21f90c4e9c3e"
		},
		"0bc3e414-59da-4af3-abd0-3740c8d22816": {
			"classDefinition": "com.sap.bpm.wfs.ui.ScriptTaskSymbol",
			"x": 335,
			"y": 86,
			"width": 100,
			"height": 60,
			"object": "e4bcba01-94c4-49dc-9e17-208cf817df80"
		},
		"273c21c9-0040-4765-aebb-614ae7471927": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "385,116 524,116",
			"sourceSymbol": "0bc3e414-59da-4af3-abd0-3740c8d22816",
			"targetSymbol": "c1d4de29-497d-4fcd-b645-1dc61b426044",
			"object": "19beab99-2ad0-488a-a3dd-a3bb8a005ede"
		},
		"62d7f4ed-4063-4c44-af8b-39050bd44926": {
			"classDefinition": "com.sap.bpm.wfs.LastIDs",
			"maildefinition": 1,
			"sequenceflow": 7,
			"startevent": 1,
			"endevent": 1,
			"usertask": 1,
			"servicetask": 1,
			"scripttask": 3,
			"mailtask": 1
		},
		"951afaee-7329-4638-86a9-18a72f265a1b": {
			"classDefinition": "com.sap.bpm.wfs.MailDefinition",
			"name": "maildefinition1",
			"to": "${context.mro_email}",
			"subject": "There’s a new vendor application form that needs your approval.",
			"text": "Hi \nA Vendor Code Application(Doc: No: ${context.no} )\nApplicant: ${context.applicant} requires  needs your approval\nVendor Name: ${context.vendorid}\n\nPlease click the following hyper link to open document for approval.\n\nhttps://wistronprework.cockpit.workflowmanagement.cfapps.eu10.hana.ondemand.com/cp.portal/site#WorkflowTask-DisplayMyInbox?sap-ui-app-id-hint=cross.fnd.fiori.inbox&substitution=true&userSearch=false&/detail/NA/ec412890-0b47-11ed-806d-eeee0a95fab7/TaskCollection(SAP__Origin='NA',InstanceID='ec412890-0b47-11ed-806d-eeee0a95fab7')\n\nNote:Once you have processed this document, the link above will become inactive!\n\nVCF(Vendor Code Form System)",
			"id": "maildefinition1"
		},
		"82d26ecf-a7ec-46db-a921-7a9ae66fcfac": {
			"classDefinition": "com.sap.bpm.wfs.ScriptTask",
			"reference": "/scripts/wf_z007_usertask/script_data.js",
			"id": "scripttask3",
			"name": "data"
		},
		"c1d4de29-497d-4fcd-b645-1dc61b426044": {
			"classDefinition": "com.sap.bpm.wfs.ui.ScriptTaskSymbol",
			"x": 474,
			"y": 86,
			"width": 100,
			"height": 60,
			"object": "82d26ecf-a7ec-46db-a921-7a9ae66fcfac"
		},
		"aa4b72d6-7380-4c52-9e4d-6befee9817ee": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow7",
			"name": "SequenceFlow7",
			"sourceRef": "82d26ecf-a7ec-46db-a921-7a9ae66fcfac",
			"targetRef": "2798f4e7-bc42-4fad-a248-159095a2f40a"
		},
		"a6cc108f-f72d-4559-8595-1658b646ae12": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "524,115.75 636.5,115.75",
			"sourceSymbol": "c1d4de29-497d-4fcd-b645-1dc61b426044",
			"targetSymbol": "53e54950-7757-4161-82c9-afa7e86cff2c",
			"object": "aa4b72d6-7380-4c52-9e4d-6befee9817ee"
		}
	}
}